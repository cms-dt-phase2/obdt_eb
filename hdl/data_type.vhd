library IEEE;
use IEEE.STD_LOGIC_1164.all;
use IEEE.NUMERIC_STD.ALL;


package data_type is

constant TDC_N : natural := 180; -- real number of TDCs (not -1)

type des_out_a    is array (0 to TDC_N-1) of std_logic_vector(9 downto 0);
type tdc_out_a    is array (0 to TDC_N-1) of std_logic_vector(29 downto 0);
type tdc_out_e_a  is array (0 to TDC_N-1) of std_logic_vector(30 downto 0);
type tdc_enc_a    is array (0 to TDC_N-1) of std_logic_vector(4 downto 0);

component ccc0
    port(
            REF_CLK_0 : in      std_logic;
            OUT0_HS_IO_CLK_0 :   out     std_logic;
            PLL_LOCK_0 :       out     std_logic
    );
end component;

component ccc1
    port(
            REF_CLK_0 : in      std_logic;
            OUT0_FABCLK_0 :   out     std_logic;
            OUT1_FABCLK_0 :   out     std_logic;
            OUT2_FABCLK_0 :   out     std_logic;
            PLL_LOCK_0 :      out     std_logic
    );
end component;

end data_type;
