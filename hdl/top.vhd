library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.NUMERIC_STD.ALL;

library polarfire;
use polarfire.all;


use work.data_type.all;

entity top is
  PORT (
    -- TDC inputs
    data_in_from_pins_p : IN std_logic_vector(TDC_N-1 downto 0);
    data_in_from_pins_n : IN std_logic_vector(TDC_N-1 downto 0);
    
    -- clocks, resets
    clk_in_p            : IN std_logic;
    clk_in_n            : IN std_logic;
    rst_n               : IN STD_LOGIC;
    
    -- Outputs
    data_out            : OUT std_logic_vector(24 downto 0);    --(to avoid synthesis pruning the logic)
    strobe_out          : OUT std_logic                         --(to avoid synthesis pruning the logic)
  );
end top;

architecture Behavioral of top is

component INBUF_DIFF
    port (PADP  : in std_logic;
          PADN  : in std_logic;
          Y     : out std_logic);
end component;


component CLKINT
    port (A : in std_logic;
          Y : out std_logic);
end component;


signal clk_p, clk_sync, clk_in, clk_in_se : std_logic := '0';
signal rst : std_logic := '0';
signal tdc_enc : tdc_enc_a :=(others=>(others=>'0'));
signal bx_cnt : std_logic_vector(11 DOWNTO 0) := (others =>'0');

signal hitdata : std_logic_vector(24 downto 0) := (others=>'0');
signal hitdata_valid : std_logic := '0';

signal dataframe : std_logic_vector(79 downto 0) := (others => '0');


begin

rst <= not rst_n;

clk_diff_input: INBUF_DIFF
    port map (
        PADN => clk_in_p,
        PADP => clk_in_n,
        Y => clk_in_se
        );

global_clk_buf: CLKINT 
    port map ( A => clk_in_se, 
               Y => clk_in
            );

ttc_inst: entity work.ttc(Behavioral)
port map ( 
    lhc_clk => clk_sync,
    rst     => rst,
    bx_cnt  => bx_cnt
  );


tdc_inst: entity work.tdc(Behavioral)
port map ( 
    -- TDC inputs
    data_in_from_pins_p => data_in_from_pins_p, 
    data_in_from_pins_n => data_in_from_pins_n,
    -- Other inputs
    bx_cnt              => bx_cnt,
    -- clocks, resets
    clk_in              => clk_in,
    rst                 => rst,
    clk_sync            => clk_sync,
    clk_p               => clk_p,
    -- TDC output
    tdc_enc             => tdc_enc
  );


--hitmux_inst: entity work.hitmux(Behavioral)
--readout_inst: entity work.readout(Behavioral)
--port map ( 
--  --clk40 => clk_sync, -- hitmux has it but Andrea's readout doesn't
--  clk => clk_p,              
--  rst => rst,
--  data_in => tdc_enc,
--  bx_cnt => bx_cnt,
--  data_out => hitdata,
--  strobe_out => hitdata_valid         
--  );

--packer_inst: entity work.packer(Behavioral)
--port map ( 
--  clk40 => clk_sync,
--  clk240 => clk_p,              
--  rst => rst,
--  hitdata => hitdata,
--  hitdata_valid => hitdata_valid,
--
--  dataframe => dataframe
--  );

----------------------------------------------------------------------
-- Output of data to dummy fpga pins to prevent synthesis optimization 
----------------------------------------------------------------------
-- version for readout + packer
--strobe_out <= hitdata_valid;  
--data_out <= dataframe( 24 + 18*to_integer(unsigned(dataframe(1 downto 0)))+1 downto 18*to_integer(unsigned(dataframe(1 downto 0)))+1 )
--            when hitdata_valid = '0' else
--            hitdata;

-- version for just TDCs
strobe_out <= '0';
data_out(24 downto 5) <= (others => '0');

process (clk_sync)
    variable index : natural range TDC_N - 1 downto 0 := 0;
begin
    if rising_edge(clk_sync) then
        data_out(4 downto 0) <= tdc_enc(index);
        index := ( index + to_integer(unsigned(tdc_enc(index))) ) mod TDC_N;
    end if;
end process;

end Behavioral;
