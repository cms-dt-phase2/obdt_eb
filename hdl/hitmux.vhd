----------------------------------------------------------------------------------
-- A. Navarro, 2018
-- 
-- 
-- 
-- 
-- 
-- 
-- 
-- 
-- 
--
-- 
-- 
-- 
-- 
----------------------------------------------------------------------------------

library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.NUMERIC_STD.ALL;

package hitmux_pkg is
  type natarray_t is array(natural range <>) of natural;
end package;


library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.NUMERIC_STD.ALL;

library work;
use work.hitmux_pkg.all;
use work.funnel_pkg.all;
use work.ttc_pkg.all;
--use work.funnel.all;
use work.data_type.all;

entity hitmux is
  Generic(
    STG_N : positive := 4;
    STGS_RATIOS : natarray_t(1 to STG_N) := (4, 4, 4, 4);
    FIFO_DEPTHS : natarray_t(1 to STG_N) := (64, 64, 256, 64)
  );
  Port ( 
    clk40 : in std_logic;
    clk   : in std_logic;
    rst   : in std_logic;
    
    -- Input data
    data_in : in tdc_enc_a;
    bx_cnt  : in std_logic_vector(11 downto 0);
    
    -- Output port
    data_out    : out std_logic_vector(24 downto 0);
    strobe_out  : out std_logic
  );
end hitmux;

architecture Behavioral of hitmux is

  type stg_info_t is record
    ratio: natural;
    number: natural; -- ceil(stage's inputs / stage's ratio). For first stage, the prev_stage's cell num is just TDC_N (default 240)
    prevstg_number : natural; -- yes, redundant info, but eases some code later on for stage 0 (tdc outputs)
    input_width: natural; -- Equals output width from previous stage
    output_width: natural; -- Equals input_width + ceil(log2(ratio))
    fifo_depth_log2 : natural;
  end record;

  type stages_info_t is array (1 to STG_N) of stg_info_t;
  
  function f_calc_stages (
    ratios : natarray_t(1 to STG_N);
    first_stg_inputs : natural;
    first_stg_input_width : natural;
    depths : natarray_t(1 to STG_N)
  ) return stages_info_t is
    variable result : stages_info_t;
  begin
    for i in 1 to STG_N loop
      result(i).ratio := ratios(i);
      
      if i = 1 then
        result(i).prevstg_number := first_stg_inputs;
        result(i).input_width := first_stg_input_width;
      else
        result(i).prevstg_number := result(i-1).number;
        result(i).input_width := result(i-1).output_width;
      end if;
      
      -- following code equivalent to result(i).number := ceil(result(i).prevstg_number / result(i).ratio)
      result(i).number := 0;
      while result(i).number * result(i).ratio < result(i).prevstg_number loop
        result(i).number := result(i).number + 1;
      end loop;
      
      result(i).output_width := result(i).input_width + f_log2( result(i).ratio );
      result(i).fifo_depth_log2 := depths(i);
    end loop;
    return result;
  end function;
  
  constant STGS_INFO: stages_info_t := f_calc_stages(  STGS_RATIOS, TDC_N, data_in(0)'length, FIFO_DEPTHS);

  constant POS_RDEN   : natural := 0;
  constant POS_EMPTY  : natural := 1;
  constant POS_HIT    : natural := 2;
  constant POS_BXLAST : natural := 3;
  constant POS_DATA   : natural := 4;

  function f_base4data ( s: natural; c: natural ) return natural is
    variable result : natural := 0;
  begin
    for stg_i in 0 to s loop
      -- for each stage output, use its output width + 4 for hit, bxlast, empty, rden
      -- version used when stg2stg only has bits for existent inputs
      if    stg_i <  s and stg_i = 0 then result := result + TDC_N                    * ( 4 + STGS_INFO(stg_i+1).input_width );
      elsif stg_i <  s and stg_i > 0 then result := result + STGS_INFO(stg_i).number  * ( 4 + STGS_INFO(stg_i).output_width  );
      elsif stg_i >= s and stg_i = 0 then result := result + c                        * ( 4 + STGS_INFO(stg_i+1).input_width );
      elsif stg_i >= s and stg_i > 0 then result := result + c                        * ( 4 + STGS_INFO(stg_i).output_width  );
      end if;
      -- version used when stg2stg has bits also for non-existent inputs
      --if    stg_i < s                 then result := result + STGS_INFO(stg_i+1).number * STGS_INFO(stg_i+1).ratio * (4 + STGS_INFO(stg_i+1).input_width);
      --elsif stg_i = 0                 then result := result + c *                                                    (4 + STGS_INFO(stg_i+1).input_width);
      --elsif stg_i >= s and stg_i > 0  then result := result + c *                                                    (4 + STGS_INFO(stg_i).output_width );
      --end if;
    end loop;
    return result;
  end function;

  signal stg2stg : std_logic_vector( f_base4data(STG_N, STGS_INFO(STG_N).number) - 1 downto 0 ); -- left value determined by first non-existent data: last stage, last cell + 1 (last cell = number - 1).
  
  signal input_strobe : std_logic;
  signal pipe_bx_cnt : std_logic_vector(11 downto 0);
  
begin

input_strobe <= bo2sl( bx_cnt = pipe_bx_cnt ) and not rst; -- strobe = 1 at the same clk edge as the new data from tdcs
pipe_bx_cnt <= bx_cnt when rising_edge(clk);


stage0_gen : for i in 0 to TDC_N - 1 generate
--stage0_gen : for i in 0 to STGS_INFO(1).number * STGS_INFO(1).ratio - 1 generate
  constant base4oport : natural := f_base4data(0, i);
  signal input_data : std_logic_vector( data_in(0)'range ) := (others => '0');
  signal oport_rden, oport_empty, empty_held : std_logic;
begin
  --inputdata_gen1 : if i < TDC_N generate
    input_data <= data_in(i);
  --end generate;
  --inputdata_gen2 : if i >= TDC_N generate
  --  input_data <= (others => '0');
  --end generate;
  -- oport_hit: determined by nonzero bin
  stg2stg( base4oport+POS_HIT ) <= bo2sl( input_data /= (input_data'range => '0') ) ;
  -- oport_bxlast: there's only none or one data per bx, so it's always lastbx
  stg2stg( base4oport+POS_BXLAST ) <= '1' ;
  -- oport_data
  stg2stg( base4oport+POS_DATA + STGS_INFO(1).input_width - 1 downto base4oport+POS_DATA ) <= input_data ;

  -- oport_rden. just rename for clearer code.
  oport_rden <= stg2stg( base4oport+POS_RDEN );
  -- oport_empty, qwfifo style; basically equals to:
  -- oport_empty <= '0' when input_strobe = '1' else '1' when oport_rden = '1' else oport_empty;
  -- but deciding the race condition in favor of input_strobe when they are coincident in the same clock cycle.
  oport_empty <= not input_strobe and (empty_held or oport_rden) ;
  empty_held <= oport_empty when rising_edge(clk);
  stg2stg( base4oport+POS_EMPTY ) <= oport_empty;
end generate;


stages_gen : for stage in 1 to STG_N generate

  cell_gen : for cell in 0 to STGS_INFO(stage).number - 1 generate
    constant base4oport : natural := f_base4data(stage, cell);
    signal iport_data : busarr_t(STGS_INFO(stage).ratio - 1 downto 0)(STGS_INFO(stage).input_width - 1 downto 0);
    signal iport_hit, iport_bxlast, iport_empty, iport_rden : std_logic_vector(STGS_INFO(stage).ratio - 1 downto 0);
    signal ff_di : std_logic_vector(STGS_INFO(stage).output_width+2 - 1 downto 0); -- +2 for hit and bxlast
    signal ff_full, ff_wren: std_logic;
  begin

    iport_gen: for iport in 0 to STGS_INFO(stage).ratio - 1 generate
      constant prevstg_c : natural := cell*STGS_INFO(stage).ratio + iport;
      constant base4iport  : natural := f_base4data(stage-1, prevstg_c);
    begin
      present_iport_gen: if prevstg_c < STGS_INFO(stage).prevstg_number generate
        stg2stg( base4iport+POS_RDEN ) <= iport_rden(iport);
        iport_empty(iport)    <= stg2stg( base4iport+POS_EMPTY );
        iport_hit(iport)      <= stg2stg( base4iport+POS_HIT );
        iport_bxlast(iport)   <= stg2stg( base4iport+POS_BXLAST );
        iport_data(iport)     <= stg2stg( base4iport+POS_DATA + STGS_INFO(stage).input_width - 1 downto base4iport+POS_DATA);
      end generate;
      absent_iport_gen: if prevstg_c >= STGS_INFO(stage).prevstg_number generate
        iport_empty(iport)    <= '0'; -- absent iport always has data ready
        iport_hit(iport)      <= '0'; -- absent iport never has hits
        iport_bxlast(iport)   <= '1'; -- absent iport data is always the last of the bx
        iport_data(iport)     <= (others => '0'); -- absent iport data is always 0
      end generate;
    end generate;


    funnel_inst : entity work.funnel(Behavioral)
    generic map(
      DATAIN_WIDTH => STGS_INFO(stage).input_width,
      IPORT_NUM => STGS_INFO(stage).ratio
    )
    port map(
      clk => clk,
      rst => rst,
      -- Input ports (QuickWord FIFOs)
      iport_rden    => iport_rden,
      iport_empty   => iport_empty,
      iport_hit     => iport_hit,
      iport_bxlast  => iport_bxlast,
      iport_data    => iport_data,
      -- Output port (FIFO)
      oport_wren    => ff_wren,
      oport_full    => ff_full,
      oport_hit     => ff_di(0),
      oport_bxlast  => ff_di(1),
      oport_data    => ff_di(2 + STGS_INFO(stage).output_width - 1 downto 2)
    );
    
    fifo_inst : entity work.pf_qwfifo(Behavioral)
    generic map(
      DATA_WIDTH => ff_di'length,
      --DEPTH_LOG2 => STGS_INFO(stage).fifo_depth_log2
      STAGE => stage
    )
    port map(
      RST   => rst,

      WRCLK => clk,
      WREN  => ff_wren,
      FULL  => ff_full,
      DI    => ff_di,
      
      RDCLK => clk,
      RDEN  => stg2stg( base4oport+POS_RDEN ),
      EMPTY => stg2stg( base4oport+POS_EMPTY ),
      DO    => stg2stg( base4oport+2 + (2 + STGS_INFO(stage).output_width) - 1 downto base4oport+2 )
    );

  end generate;
end generate;


-- Output fifo is always being read


output_gen: if True generate
  constant base4iport : natural := f_base4data(STG_N, 0);
  signal bx_out : std_logic_vector(11 downto 0);
  signal fine_out : std_logic_vector(data_in(0)'range);
  signal chan_out : std_logic_vector( STGS_INFO(STG_N).output_width - data_in(0)'length -1 downto 0) ;
begin

  -- re-builds the bx counter of the output data. It is free-running since rst, it should be made to re-sync periodically by 
  --   * propagating a bc0 signal through the funnels: advantage it also resyncs any possible lost last_bx, disadvantage it takes a lot of storage resources.
  --   * making a fifo for the bunch counter as a kind of pipeline.
  process (clk) 
    variable bx_valid : boolean := False;
  begin
    if rising_edge(clk) then 
      if rst='1' then
        bx_valid := False;
        bx_out <= (others => '1');
      else
        if not bx_valid and input_strobe = '1' then
          bx_valid := True;
          bx_out <= bx_cnt;
        elsif bx_valid and stg2stg( base4iport+POS_BXLAST ) = '1' and stg2stg( base4iport+POS_EMPTY ) = '0' then
          if bx_out = std_logic_vector(to_unsigned(LHC_BUNCH_COUNT,12)) then bx_out <= (others => '0');
          else bx_out <= std_logic_vector(unsigned(bx_out)+1);
          end if;
        end if;
      end if;
    end if;
  end process;

  stg2stg( base4iport+POS_RDEN ) <= '1';
  
  fine_out    <= stg2stg( base4iport+POS_DATA + data_in(0)'length - 1 downto base4iport+POS_DATA);
  chan_out    <= stg2stg( base4iport+POS_DATA + STGS_INFO(STG_N).output_width - 1 downto base4iport+POS_DATA + data_in(0)'length);

  data_out(fine_out'length + bx_out'length + chan_out'length - 1 downto 0) <= fine_out & bx_out & chan_out;
  strobe_out  <= not stg2stg( base4iport+POS_EMPTY ) and stg2stg( base4iport+POS_HIT );

end generate;


end Behavioral;
