library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.NUMERIC_STD.ALL;
--use ieee.std_logic_misc.all;

library polarfire;
use polarfire.all;

use work.data_type.all;

entity tdc is
  PORT (
    -- TDC inputs
    data_in_from_pins_p : IN std_logic_vector(TDC_N-1 downto 0);
    data_in_from_pins_n : IN std_logic_vector(TDC_N-1 downto 0);
    -- Other inputs
    bx_cnt : in std_logic_vector(11 downto 0);
    -- clocks, resets
    clk_in              : IN std_logic;
    rst                 : IN STD_LOGIC;
    clk_sync            : out std_logic;
    clk_p               : out std_logic;
    -- TDC output
    tdc_enc : OUT tdc_enc_a 
  );
end tdc;

architecture Behavioral of tdc is


component ides_36
    -- Port list
    port(
    -- Inputs
    ARST_N          : in  std_logic;
    HS_IO_CLK_PAUSE : in  std_logic;
    RXD             : in  std_logic_vector(35 downto 0);
    RXD_N           : in  std_logic_vector(35 downto 0);
    RX_CLK          : in  std_logic;
    -- Outputs
    L0_RXD_DATA     : out std_logic_vector(9 downto 0);
    L1_RXD_DATA     : out std_logic_vector(9 downto 0);
    L2_RXD_DATA     : out std_logic_vector(9 downto 0);
    L3_RXD_DATA     : out std_logic_vector(9 downto 0);
    L4_RXD_DATA     : out std_logic_vector(9 downto 0);
    L5_RXD_DATA     : out std_logic_vector(9 downto 0);
    L6_RXD_DATA     : out std_logic_vector(9 downto 0);
    L7_RXD_DATA     : out std_logic_vector(9 downto 0);
    L8_RXD_DATA     : out std_logic_vector(9 downto 0);
    L9_RXD_DATA     : out std_logic_vector(9 downto 0);
    L10_RXD_DATA    : out std_logic_vector(9 downto 0);
    L11_RXD_DATA    : out std_logic_vector(9 downto 0);
    L12_RXD_DATA    : out std_logic_vector(9 downto 0);
    L13_RXD_DATA    : out std_logic_vector(9 downto 0);
    L14_RXD_DATA    : out std_logic_vector(9 downto 0);
    L15_RXD_DATA    : out std_logic_vector(9 downto 0);
    L16_RXD_DATA    : out std_logic_vector(9 downto 0);
    L17_RXD_DATA    : out std_logic_vector(9 downto 0);
    L18_RXD_DATA    : out std_logic_vector(9 downto 0);
    L19_RXD_DATA    : out std_logic_vector(9 downto 0);
    L20_RXD_DATA    : out std_logic_vector(9 downto 0);
    L21_RXD_DATA    : out std_logic_vector(9 downto 0);
    L22_RXD_DATA    : out std_logic_vector(9 downto 0);
    L23_RXD_DATA    : out std_logic_vector(9 downto 0);
    L24_RXD_DATA    : out std_logic_vector(9 downto 0);
    L25_RXD_DATA    : out std_logic_vector(9 downto 0);
    L26_RXD_DATA    : out std_logic_vector(9 downto 0);
    L27_RXD_DATA    : out std_logic_vector(9 downto 0);
    L28_RXD_DATA    : out std_logic_vector(9 downto 0);
    L29_RXD_DATA    : out std_logic_vector(9 downto 0);
    L30_RXD_DATA    : out std_logic_vector(9 downto 0);
    L31_RXD_DATA    : out std_logic_vector(9 downto 0);
    L32_RXD_DATA    : out std_logic_vector(9 downto 0);
    L33_RXD_DATA    : out std_logic_vector(9 downto 0);
    L34_RXD_DATA    : out std_logic_vector(9 downto 0);
    L35_RXD_DATA    : out std_logic_vector(9 downto 0);
    RX_CLK_G        : out std_logic
);
end component;


component ides_19
    -- Port list
    port(
    -- Inputs
    ARST_N          : in  std_logic;
    HS_IO_CLK_PAUSE : in  std_logic;
    RXD             : in  std_logic_vector(18 downto 0);
    RXD_N           : in  std_logic_vector(18 downto 0);
    RX_CLK          : in  std_logic;
    -- Outputs
    L0_RXD_DATA     : out std_logic_vector(9 downto 0);
    L1_RXD_DATA     : out std_logic_vector(9 downto 0);
    L2_RXD_DATA     : out std_logic_vector(9 downto 0);
    L3_RXD_DATA     : out std_logic_vector(9 downto 0);
    L4_RXD_DATA     : out std_logic_vector(9 downto 0);
    L5_RXD_DATA     : out std_logic_vector(9 downto 0);
    L6_RXD_DATA     : out std_logic_vector(9 downto 0);
    L7_RXD_DATA     : out std_logic_vector(9 downto 0);
    L8_RXD_DATA     : out std_logic_vector(9 downto 0);
    L9_RXD_DATA     : out std_logic_vector(9 downto 0);
    L10_RXD_DATA    : out std_logic_vector(9 downto 0);
    L11_RXD_DATA    : out std_logic_vector(9 downto 0);
    L12_RXD_DATA    : out std_logic_vector(9 downto 0);
    L13_RXD_DATA    : out std_logic_vector(9 downto 0);
    L14_RXD_DATA    : out std_logic_vector(9 downto 0);
    L15_RXD_DATA    : out std_logic_vector(9 downto 0);
    L16_RXD_DATA    : out std_logic_vector(9 downto 0);
    L17_RXD_DATA    : out std_logic_vector(9 downto 0);
    L18_RXD_DATA    : out std_logic_vector(9 downto 0);
    RX_CLK_G        : out std_logic
);
end component;

component ides_24
    -- Port list
    port(
    -- Inputs
    ARST_N          : in  std_logic;
    HS_IO_CLK_PAUSE : in  std_logic;
    RXD             : in  std_logic_vector(23 downto 0);
    RXD_N           : in  std_logic_vector(23 downto 0);
    RX_CLK          : in  std_logic;
    -- Outputs
    L0_RXD_DATA     : out std_logic_vector(9 downto 0);
    L1_RXD_DATA     : out std_logic_vector(9 downto 0);
    L2_RXD_DATA     : out std_logic_vector(9 downto 0);
    L3_RXD_DATA     : out std_logic_vector(9 downto 0);
    L4_RXD_DATA     : out std_logic_vector(9 downto 0);
    L5_RXD_DATA     : out std_logic_vector(9 downto 0);
    L6_RXD_DATA     : out std_logic_vector(9 downto 0);
    L7_RXD_DATA     : out std_logic_vector(9 downto 0);
    L8_RXD_DATA     : out std_logic_vector(9 downto 0);
    L9_RXD_DATA     : out std_logic_vector(9 downto 0);
    L10_RXD_DATA    : out std_logic_vector(9 downto 0);
    L11_RXD_DATA    : out std_logic_vector(9 downto 0);
    L12_RXD_DATA    : out std_logic_vector(9 downto 0);
    L13_RXD_DATA    : out std_logic_vector(9 downto 0);
    L14_RXD_DATA    : out std_logic_vector(9 downto 0);
    L15_RXD_DATA    : out std_logic_vector(9 downto 0);
    L16_RXD_DATA    : out std_logic_vector(9 downto 0);
    L17_RXD_DATA    : out std_logic_vector(9 downto 0);
    L18_RXD_DATA    : out std_logic_vector(9 downto 0);
    L19_RXD_DATA    : out std_logic_vector(9 downto 0);
    L20_RXD_DATA    : out std_logic_vector(9 downto 0);
    L21_RXD_DATA    : out std_logic_vector(9 downto 0);
    L22_RXD_DATA    : out std_logic_vector(9 downto 0);
    L23_RXD_DATA    : out std_logic_vector(9 downto 0);
    RX_CLK_G        : out std_logic
);
end component;



component ides_43
    -- Port list
    port(
    -- Inputs
    ARST_N          : in  std_logic;
    HS_IO_CLK_PAUSE : in  std_logic;
    RXD             : in  std_logic_vector(42 downto 0);
    RXD_N           : in  std_logic_vector(42 downto 0);
    RX_CLK          : in  std_logic;
    -- Outputs
    L0_RXD_DATA     : out std_logic_vector(9 downto 0);
    L1_RXD_DATA     : out std_logic_vector(9 downto 0);
    L2_RXD_DATA     : out std_logic_vector(9 downto 0);
    L3_RXD_DATA     : out std_logic_vector(9 downto 0);
    L4_RXD_DATA     : out std_logic_vector(9 downto 0);
    L5_RXD_DATA     : out std_logic_vector(9 downto 0);
    L6_RXD_DATA     : out std_logic_vector(9 downto 0);
    L7_RXD_DATA     : out std_logic_vector(9 downto 0);
    L8_RXD_DATA     : out std_logic_vector(9 downto 0);
    L9_RXD_DATA     : out std_logic_vector(9 downto 0);
    L10_RXD_DATA    : out std_logic_vector(9 downto 0);
    L11_RXD_DATA    : out std_logic_vector(9 downto 0);
    L12_RXD_DATA    : out std_logic_vector(9 downto 0);
    L13_RXD_DATA    : out std_logic_vector(9 downto 0);
    L14_RXD_DATA    : out std_logic_vector(9 downto 0);
    L15_RXD_DATA    : out std_logic_vector(9 downto 0);
    L16_RXD_DATA    : out std_logic_vector(9 downto 0);
    L17_RXD_DATA    : out std_logic_vector(9 downto 0);
    L18_RXD_DATA    : out std_logic_vector(9 downto 0);
    L19_RXD_DATA    : out std_logic_vector(9 downto 0);
    L20_RXD_DATA    : out std_logic_vector(9 downto 0);
    L21_RXD_DATA    : out std_logic_vector(9 downto 0);
    L22_RXD_DATA    : out std_logic_vector(9 downto 0);
    L23_RXD_DATA    : out std_logic_vector(9 downto 0);
    L24_RXD_DATA    : out std_logic_vector(9 downto 0);
    L25_RXD_DATA    : out std_logic_vector(9 downto 0);
    L26_RXD_DATA    : out std_logic_vector(9 downto 0);
    L27_RXD_DATA    : out std_logic_vector(9 downto 0);
    L28_RXD_DATA    : out std_logic_vector(9 downto 0);
    L29_RXD_DATA    : out std_logic_vector(9 downto 0);
    L30_RXD_DATA    : out std_logic_vector(9 downto 0);
    L31_RXD_DATA    : out std_logic_vector(9 downto 0);
    L32_RXD_DATA    : out std_logic_vector(9 downto 0);
    L33_RXD_DATA    : out std_logic_vector(9 downto 0);
    L34_RXD_DATA    : out std_logic_vector(9 downto 0);
    L35_RXD_DATA    : out std_logic_vector(9 downto 0);
    L36_RXD_DATA    : out std_logic_vector(9 downto 0);
    L37_RXD_DATA    : out std_logic_vector(9 downto 0);
    L38_RXD_DATA    : out std_logic_vector(9 downto 0);
    L39_RXD_DATA    : out std_logic_vector(9 downto 0);
    L40_RXD_DATA    : out std_logic_vector(9 downto 0);
    L41_RXD_DATA    : out std_logic_vector(9 downto 0);
    L42_RXD_DATA    : out std_logic_vector(9 downto 0);
    RX_CLK_G        : out std_logic
);
end component;


component ides_46
    -- Port list
    port(
    -- Inputs
    ARST_N          : in  std_logic;
    HS_IO_CLK_PAUSE : in  std_logic;
    RXD             : in  std_logic_vector(45 downto 0);
    RXD_N           : in  std_logic_vector(45 downto 0);
    RX_CLK          : in  std_logic;
    -- Outputs
    L0_RXD_DATA     : out std_logic_vector(9 downto 0);
    L1_RXD_DATA     : out std_logic_vector(9 downto 0);
    L2_RXD_DATA     : out std_logic_vector(9 downto 0);
    L3_RXD_DATA     : out std_logic_vector(9 downto 0);
    L4_RXD_DATA     : out std_logic_vector(9 downto 0);
    L5_RXD_DATA     : out std_logic_vector(9 downto 0);
    L6_RXD_DATA     : out std_logic_vector(9 downto 0);
    L7_RXD_DATA     : out std_logic_vector(9 downto 0);
    L8_RXD_DATA     : out std_logic_vector(9 downto 0);
    L9_RXD_DATA     : out std_logic_vector(9 downto 0);
    L10_RXD_DATA    : out std_logic_vector(9 downto 0);
    L11_RXD_DATA    : out std_logic_vector(9 downto 0);
    L12_RXD_DATA    : out std_logic_vector(9 downto 0);
    L13_RXD_DATA    : out std_logic_vector(9 downto 0);
    L14_RXD_DATA    : out std_logic_vector(9 downto 0);
    L15_RXD_DATA    : out std_logic_vector(9 downto 0);
    L16_RXD_DATA    : out std_logic_vector(9 downto 0);
    L17_RXD_DATA    : out std_logic_vector(9 downto 0);
    L18_RXD_DATA    : out std_logic_vector(9 downto 0);
    L19_RXD_DATA    : out std_logic_vector(9 downto 0);
    L20_RXD_DATA    : out std_logic_vector(9 downto 0);
    L21_RXD_DATA    : out std_logic_vector(9 downto 0);
    L22_RXD_DATA    : out std_logic_vector(9 downto 0);
    L23_RXD_DATA    : out std_logic_vector(9 downto 0);
    L24_RXD_DATA    : out std_logic_vector(9 downto 0);
    L25_RXD_DATA    : out std_logic_vector(9 downto 0);
    L26_RXD_DATA    : out std_logic_vector(9 downto 0);
    L27_RXD_DATA    : out std_logic_vector(9 downto 0);
    L28_RXD_DATA    : out std_logic_vector(9 downto 0);
    L29_RXD_DATA    : out std_logic_vector(9 downto 0);
    L30_RXD_DATA    : out std_logic_vector(9 downto 0);
    L31_RXD_DATA    : out std_logic_vector(9 downto 0);
    L32_RXD_DATA    : out std_logic_vector(9 downto 0);
    L33_RXD_DATA    : out std_logic_vector(9 downto 0);
    L34_RXD_DATA    : out std_logic_vector(9 downto 0);
    L35_RXD_DATA    : out std_logic_vector(9 downto 0);
    L36_RXD_DATA    : out std_logic_vector(9 downto 0);
    L37_RXD_DATA    : out std_logic_vector(9 downto 0);
    L38_RXD_DATA    : out std_logic_vector(9 downto 0);
    L39_RXD_DATA    : out std_logic_vector(9 downto 0);
    L40_RXD_DATA    : out std_logic_vector(9 downto 0);
    L41_RXD_DATA    : out std_logic_vector(9 downto 0);
    L42_RXD_DATA    : out std_logic_vector(9 downto 0);
    L43_RXD_DATA    : out std_logic_vector(9 downto 0);
    L44_RXD_DATA    : out std_logic_vector(9 downto 0);
    L45_RXD_DATA    : out std_logic_vector(9 downto 0);
    RX_CLK_G        : out std_logic
);
end component;

component CLKINT
    port (A : in std_logic;
          Y : out std_logic);
end component;

signal clk_s, clk_f, clk_s_r, clk_f_nonbuffered : std_logic :='0';

signal tdc_out_r, mask : std_logic_vector(TDC_N-1 downto 0) :=(others=>'0');
signal des_out : des_out_a :=(others=>(others=>'0'));
signal tdc_out, tdc_edge : tdc_out_a :=(others=>(others=>'0'));
signal tdc_out_e : tdc_out_e_a :=(others=>(others=>'0'));
signal rst_io_auto : std_logic :='0';

signal locked, locked1 : std_logic :='0';

signal tp : std_logic_vector(1 downto 0) :=(others=>'0');
signal tp_cnt : std_logic_vector(15 downto 0) :=(others=>'0');
signal tp_d, tp_d_l, tp_prescale, tp_d_2, tp_d_2_l, tp_prescale_2 : std_logic :='0';
signal sequence_i : std_logic_vector(2 downto 0);
signal cnt : integer range 0 to 15 :=0;
signal clk_des : std_logic_vector(TDC_N-1 downto 0) :=(others=>'0');

signal or_cnt : STD_LOGIC_VECTOR(31 DOWNTO 0) :=(others =>'0');

begin

ccc_0 : ccc0
    PORT MAP (
        -- Inputs
        REF_CLK_0 => clk_in,
        -- Outputs
        OUT0_HS_IO_CLK_0 => clk_f_nonbuffered,
        PLL_LOCK_0 => locked
    ); 


GLOBAL_CLOCK_BUFFER: CLKINT 
    port map ( A => clk_f_nonbuffered, 
               Y => clk_f
            );


ccc_1 : ccc1
    PORT MAP (
        -- Inputs
        REF_CLK_0 => clk_s_r,
        -- Outputs
        OUT0_FABCLK_0 => clk_sync,
        OUT1_FABCLK_0 => clk_p,
        OUT2_FABCLK_0 => clk_s,
        PLL_LOCK_0 => locked1
    ); 



--------- Reset SerDes -------------------------------------------------------------

rst_des_p: process(rst, clk_in)
begin
	if rst = '1' then
		rst_io_auto <= '0';
		cnt <= 0;
	elsif (clk_in='1' and clk_in'event) then
		if locked = '1' and cnt < 10 then
			rst_io_auto <= '1';
			cnt <= cnt + 1;
		else
			rst_io_auto <= '0';
			cnt <= cnt;
		end if;
	end if;
end process;

------------------------------------------------------------------------------------

des_36_bank0 : ides_36
   port map(
    -- Inputs
    ARST_N          => not rst_io_auto,
    HS_IO_CLK_PAUSE => '0',
    RXD  => data_in_from_pins_p(35 downto 0),
    RXD_N  => data_in_from_pins_n(35 downto 0),
    RX_CLK          => clk_f,
    -- Outputs
    L0_RXD_DATA     => des_out(0),
    L1_RXD_DATA     => des_out(1),
    L2_RXD_DATA     => des_out(2),
    L3_RXD_DATA     => des_out(3),
    L4_RXD_DATA     => des_out(4),
    L5_RXD_DATA     => des_out(5),
    L6_RXD_DATA     => des_out(6),
    L7_RXD_DATA     => des_out(7),
    L8_RXD_DATA     => des_out(8),
    L9_RXD_DATA     => des_out(9),
    L10_RXD_DATA    => des_out(10),
    L11_RXD_DATA    => des_out(11),
    L12_RXD_DATA    => des_out(12),
    L13_RXD_DATA    => des_out(13),
    L14_RXD_DATA    => des_out(14),
    L15_RXD_DATA    => des_out(15),
    L16_RXD_DATA    => des_out(16),
    L17_RXD_DATA    => des_out(17),
    L18_RXD_DATA    => des_out(18),
    L19_RXD_DATA    => des_out(19),
    L20_RXD_DATA    => des_out(20),
    L21_RXD_DATA    => des_out(21),
    L22_RXD_DATA    => des_out(22),
    L23_RXD_DATA    => des_out(23),
    L24_RXD_DATA    => des_out(24),
    L25_RXD_DATA    => des_out(25),
    L26_RXD_DATA    => des_out(26),
    L27_RXD_DATA    => des_out(27),
    L28_RXD_DATA    => des_out(28),
    L29_RXD_DATA    => des_out(29),
    L30_RXD_DATA    => des_out(30),
    L31_RXD_DATA    => des_out(31),
    L32_RXD_DATA    => des_out(32),
    L33_RXD_DATA    => des_out(33),
    L34_RXD_DATA    => des_out(34),
    L35_RXD_DATA    => des_out(35),
    RX_CLK_G        => open
);  


------------------------------------------------------------------------------------

des_19_bank1 : ides_19
   port map(
    -- Inputs
    ARST_N          => not rst_io_auto,
    HS_IO_CLK_PAUSE => '0',
    RXD  => data_in_from_pins_p(54 downto 36),
    RXD_N  => data_in_from_pins_n(54 downto 36),
    RX_CLK          => clk_f,
    -- Outputs
    L0_RXD_DATA     => des_out(36),
    L1_RXD_DATA     => des_out(37),
    L2_RXD_DATA     => des_out(38),
    L3_RXD_DATA     => des_out(39),
    L4_RXD_DATA     => des_out(40),
    L5_RXD_DATA     => des_out(41),
    L6_RXD_DATA     => des_out(42),
    L7_RXD_DATA     => des_out(43),
    L8_RXD_DATA     => des_out(44),
    L9_RXD_DATA     => des_out(45),
    L10_RXD_DATA    => des_out(46),
    L11_RXD_DATA    => des_out(47),
    L12_RXD_DATA    => des_out(48),
    L13_RXD_DATA    => des_out(49),
    L14_RXD_DATA    => des_out(50),
    L15_RXD_DATA    => des_out(51),
    L16_RXD_DATA    => des_out(52),
    L17_RXD_DATA    => des_out(53),
    L18_RXD_DATA    => des_out(54),
    RX_CLK_G        => open
);  
------------------------------------------------------------------------------------

des_43_bank2 : ides_43
   port map(
    -- Inputs
    ARST_N          => not rst_io_auto,
    HS_IO_CLK_PAUSE => '0',
    RXD  => data_in_from_pins_p(97 downto 55),
    RXD_N  => data_in_from_pins_n(97 downto 55),
    RX_CLK          => clk_f,
    -- Outputs
    L0_RXD_DATA     => des_out(55),
    L1_RXD_DATA     => des_out(56),    
    L2_RXD_DATA     => des_out(57),
    L3_RXD_DATA     => des_out(58),
    L4_RXD_DATA     => des_out(59),
    L5_RXD_DATA     => des_out(60),
    L6_RXD_DATA     => des_out(61),
    L7_RXD_DATA     => des_out(62),
    L8_RXD_DATA     => des_out(63),
    L9_RXD_DATA     => des_out(64),
    L10_RXD_DATA    => des_out(65),
    L11_RXD_DATA    => des_out(66),
    L12_RXD_DATA    => des_out(67),
    L13_RXD_DATA    => des_out(68),
    L14_RXD_DATA    => des_out(69),
    L15_RXD_DATA    => des_out(70),
    L16_RXD_DATA    => des_out(71),
    L17_RXD_DATA    => des_out(72),
    L18_RXD_DATA    => des_out(73),
    L19_RXD_DATA    => des_out(74),
    L20_RXD_DATA    => des_out(75),
    L21_RXD_DATA    => des_out(76),
    L22_RXD_DATA    => des_out(77),
    L23_RXD_DATA    => des_out(78),
    L24_RXD_DATA    => des_out(79),
    L25_RXD_DATA    => des_out(80),
    L26_RXD_DATA    => des_out(81),
    L27_RXD_DATA    => des_out(82),
    L28_RXD_DATA    => des_out(83),
    L29_RXD_DATA    => des_out(84),
    L30_RXD_DATA    => des_out(85),
    L31_RXD_DATA    => des_out(86),
    L32_RXD_DATA    => des_out(87),
    L33_RXD_DATA    => des_out(88),
    L34_RXD_DATA    => des_out(89),
    L35_RXD_DATA    => des_out(90),
    L36_RXD_DATA    => des_out(91),
    L37_RXD_DATA    => des_out(92),
    L38_RXD_DATA    => des_out(93),
    L39_RXD_DATA    => des_out(94),
    L40_RXD_DATA    => des_out(95),
    L41_RXD_DATA    => des_out(96),
    L42_RXD_DATA    => des_out(97),
    RX_CLK_G        => clk_des(0)
);  

------------------------------------------------------------------------------------

des_46_bank4 : ides_46
   port map(
    -- Inputs
    ARST_N          => not rst_io_auto,
    HS_IO_CLK_PAUSE => '0',
    RXD  => data_in_from_pins_p(143 downto 98),
    RXD_N  => data_in_from_pins_n(143 downto 98),
    RX_CLK          => clk_f,
    -- Outputs
    L0_RXD_DATA     => des_out(98),
    L1_RXD_DATA     => des_out(99),    
    L2_RXD_DATA     => des_out(100),
    L3_RXD_DATA     => des_out(101),
    L4_RXD_DATA     => des_out(102),
    L5_RXD_DATA     => des_out(103),
    L6_RXD_DATA     => des_out(104),
    L7_RXD_DATA     => des_out(105),
    L8_RXD_DATA     => des_out(106),
    L9_RXD_DATA     => des_out(107),
    L10_RXD_DATA    => des_out(108),
    L11_RXD_DATA    => des_out(109),
    L12_RXD_DATA    => des_out(110),
    L13_RXD_DATA    => des_out(111),
    L14_RXD_DATA    => des_out(112),
    L15_RXD_DATA    => des_out(113),
    L16_RXD_DATA    => des_out(114),
    L17_RXD_DATA    => des_out(115),
    L18_RXD_DATA    => des_out(116),
    L19_RXD_DATA    => des_out(117),
    L20_RXD_DATA    => des_out(118),
    L21_RXD_DATA    => des_out(119),
    L22_RXD_DATA    => des_out(120),
    L23_RXD_DATA    => des_out(121),
    L24_RXD_DATA    => des_out(122),
    L25_RXD_DATA    => des_out(123),
    L26_RXD_DATA    => des_out(124),
    L27_RXD_DATA    => des_out(125),
    L28_RXD_DATA    => des_out(126),
    L29_RXD_DATA    => des_out(127),
    L30_RXD_DATA    => des_out(128),
    L31_RXD_DATA    => des_out(129),
    L32_RXD_DATA    => des_out(130),
    L33_RXD_DATA    => des_out(131),
    L34_RXD_DATA    => des_out(132),
    L35_RXD_DATA    => des_out(133),
    L36_RXD_DATA    => des_out(134),
    L37_RXD_DATA    => des_out(135),
    L38_RXD_DATA    => des_out(136),
    L39_RXD_DATA    => des_out(137),
    L40_RXD_DATA    => des_out(138),
    L41_RXD_DATA    => des_out(139),
    L42_RXD_DATA    => des_out(140),
    L43_RXD_DATA    => des_out(141),
    L44_RXD_DATA    => des_out(142),
    L45_RXD_DATA    => des_out(143),
    RX_CLK_G        => open
);  

------------------------------------------------------------------------------------

--des_24_bank5 : ides_24
   --port map(
    ---- Inputs
    --ARST_N          => not rst_io_auto,
    --HS_IO_CLK_PAUSE => '0',
    --RXD    => data_in_from_pins_p(167 downto 144),
    --RXD_N  => data_in_from_pins_n(167 downto 144),
    --RX_CLK          => clk_f,
    ---- Outputs
    --L0_RXD_DATA     => des_out(144),
    --L1_RXD_DATA     => des_out(145),
    --L2_RXD_DATA     => des_out(146),
    --L3_RXD_DATA     => des_out(147),
    --L4_RXD_DATA     => des_out(148),
    --L5_RXD_DATA     => des_out(149),
    --L6_RXD_DATA     => des_out(150),
    --L7_RXD_DATA     => des_out(151),
    --L8_RXD_DATA     => des_out(152),
    --L9_RXD_DATA     => des_out(153),
    --L10_RXD_DATA    => des_out(154),
    --L11_RXD_DATA    => des_out(155),
    --L12_RXD_DATA    => des_out(156),
    --L13_RXD_DATA    => des_out(157),
    --L14_RXD_DATA    => des_out(158),
    --L15_RXD_DATA    => des_out(159),
    --L16_RXD_DATA    => des_out(160),
    --L17_RXD_DATA    => des_out(161),
    --L18_RXD_DATA    => des_out(162),
    --L19_RXD_DATA    => des_out(163),
    --L20_RXD_DATA    => des_out(164),
    --L21_RXD_DATA    => des_out(165),
    --L22_RXD_DATA    => des_out(166),
    --L23_RXD_DATA    => des_out(167),
    --RX_CLK_G        => open
--);  

------------------------------------------------------------------------------------

des_36_bank6 : ides_36
   port map(
    -- Inputs
    ARST_N          => not rst_io_auto,
    HS_IO_CLK_PAUSE => '0',
    RXD  => data_in_from_pins_p(179 downto 144),
    RXD_N  => data_in_from_pins_n(179 downto 144),
    RX_CLK          => clk_f,
    -- Outputs
    L0_RXD_DATA     => des_out(144),
    L1_RXD_DATA     => des_out(145),
    L2_RXD_DATA     => des_out(146),
    L3_RXD_DATA     => des_out(147),
    L4_RXD_DATA     => des_out(148),
    L5_RXD_DATA     => des_out(149),
    L6_RXD_DATA     => des_out(150),
    L7_RXD_DATA     => des_out(151),
    L8_RXD_DATA     => des_out(152),
    L9_RXD_DATA     => des_out(153),
    L10_RXD_DATA    => des_out(154),
    L11_RXD_DATA    => des_out(155),
    L12_RXD_DATA    => des_out(156),
    L13_RXD_DATA    => des_out(157),
    L14_RXD_DATA    => des_out(158),
    L15_RXD_DATA    => des_out(159),
    L16_RXD_DATA    => des_out(160),
    L17_RXD_DATA    => des_out(161),
    L18_RXD_DATA    => des_out(162),
    L19_RXD_DATA    => des_out(163),
    L20_RXD_DATA    => des_out(164),
    L21_RXD_DATA    => des_out(165),
    L22_RXD_DATA    => des_out(166),
    L23_RXD_DATA    => des_out(167),
    L24_RXD_DATA    => des_out(168),
    L25_RXD_DATA    => des_out(169),
    L26_RXD_DATA    => des_out(170),
    L27_RXD_DATA    => des_out(171),
    L28_RXD_DATA    => des_out(172),
    L29_RXD_DATA    => des_out(173),
    L30_RXD_DATA    => des_out(174),
    L31_RXD_DATA    => des_out(175),
    L32_RXD_DATA    => des_out(176),
    L33_RXD_DATA    => des_out(177),
    L34_RXD_DATA    => des_out(178),
    L35_RXD_DATA    => des_out(179),
    RX_CLK_G        => open
);  

------------------------------------------------------------------------------------




clk_s_r <= clk_des(0);
  
shift_word_p : process(clk_s, rst)
begin
    if rst = '1' then
        tdc_out <= (others=>(others=>'0'));
    elsif clk_s'event and clk_s = '1' then
        for i in 0 to TDC_N-1 loop
            tdc_out(i)(19 downto 0) <= tdc_out(i)(29 downto 10);
            tdc_out(i)(29 downto 20) <= des_out(i);
        end loop;
    end if;
end process;       

tdc_out_e_g : for i in 0 to TDC_N-1 generate
begin
    tdc_out_e(i) <= tdc_out(i) & tdc_out_r(i);
end generate;

tdc_edge_g_g : for i in 0 to TDC_N-1 generate
    tdc_edge_g : for j in 0 to 29 generate
        tdc_edge(i)(j) <= '1' when tdc_out_e(i)(j+1) = '1' and tdc_out_e(i)(j) = '0' else
                          '0';
    end generate;
end generate;

encoder_p : process (clk_sync, rst, tdc_edge, tdc_out)
    type tdc_out_enc_a is array (0 to TDC_N-1) of integer range 0 to 30;
    variable tdc_out_enc : tdc_out_enc_a :=(others=>0);
begin
    if rst= '1' then
        tdc_out_enc := (others=>0);
        tdc_out_r <= (others=>'0');
        tdc_enc <= (others=>(others=>'0'));
    elsif clk_sync'event and clk_sync = '1' then
        for i in 0 to TDC_N-1 loop
            tdc_out_r(i) <= tdc_out(i)(29);
            tdc_out_enc(i) := 0;
            for j in 0 to 29 loop
                if tdc_edge(i)(29-j) = '1' then
                    tdc_out_enc(i) := 30-j;
                else 
                    tdc_out_enc(i) := tdc_out_enc(i);
                end if;
            end loop;
            if mask(i) = '0' then
                if tp = "00" then
                    tdc_enc(i) <= std_logic_vector(to_unsigned(tdc_out_enc(i),5));
                elsif tp = "01" then
                    if tp_prescale = '1' then
                        tdc_enc(i) <= '0' & sequence_i & '1';
                        sequence_i <= std_logic_vector(unsigned(sequence_i) + 1);
                    else
                        tdc_enc(i) <= (others=>'0');
                        sequence_i <= sequence_i;
                    end if;
                elsif tp = "10" then
                    if bx_cnt = x"190" and or_cnt(6 downto 0) = "1111111" then
                        tdc_enc(i) <= "00001";
                    else
                        tdc_enc(i) <= (others=>'0');
                    end if;
                else
                    if bx_cnt = x"190" then
                        tdc_enc(i) <= "00001";
                    else
                        tdc_enc(i) <= (others=>'0');
                    end if;    
                end if;    
            else
                tdc_enc(i) <= (others=>'0');
            end if;    
        end loop;            
    end if;
end process; 

tp_d <= '1' when tp_cnt(6 downto 0) = "1111111" else
        '0'; 
tp_d_2 <= '1' when tp_cnt(4 downto 0) = "11111" else
        '0';      
tp_prescale <= '1' when tp_d ='1' and tp_d_l = '0' else
               '0';
tp_prescale_2 <= '1' when tp_d_2 ='1' and tp_d_2_l = '0' else
                 '0';                       

tp_cnt_p : process (clk_sync, rst, tp_cnt)
begin
    if rst = '1' then
        tp_cnt <= (others=>'0');
    elsif clk_sync'event and clk_sync = '1' then
        tp_d_l <= tp_d;
        tp_d_2_l <= tp_d_2;
        if bx_cnt = x"190" then
            tp_cnt <= std_logic_vector(unsigned(tp_cnt) + 1); 
        else
            tp_cnt <= tp_cnt;
        end if;
    end if;
end process;

end Behavioral;