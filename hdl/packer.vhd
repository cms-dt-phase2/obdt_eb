----------------------------------------------------------------------------------
-- A. Navarro, 2018
-- 
-- Builds the data frame for the transceiver block
-- For the moment, it just puts as much as 3 valid hits in a 80-bit dataframe.
-- If more than 3 valid hits come, it just discards them.
-- 
----------------------------------------------------------------------------------

library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.NUMERIC_STD.ALL;

entity packer is
  Port ( 
    clk40 : in std_logic;
    clk240 : in std_logic;
    rst     : in std_logic;
    hitdata : in std_logic_vector(24 downto 0);
    hitdata_valid : in std_logic;
    
    dataframe : out std_logic_vector(79 downto 0)
  );
end packer;

architecture Behavioral of packer is

signal tictoc20 : std_logic;


begin


tictoc20 <= not tictoc20 when rising_edge(clk40);  


process(clk240)
  variable tictoc20_pipe : std_logic := '0';
  variable dataframe_v : std_logic_vector(79 downto 0) := (others => '0');
  variable hitnum : unsigned(1 downto 0) := "00";
begin
  if rising_edge(clk240) then
    if rst = '1' then
      tictoc20_pipe := tictoc20;
      dataframe_v := (others => '0');
      dataframe <= dataframe_v;
      hitnum := "00";
    else
      if hitdata_valid = '1' and hitnum /= "11" then
        dataframe_v(24 + 25*to_integer(hitnum) downto 25*to_integer(hitnum)) := hitdata;
        hitnum := hitnum + 1;
      end if;
      
      if tictoc20 /= tictoc20_pipe then
        dataframe <= dataframe_v;
        dataframe_v := (others => '0');
        hitnum := "00";
      end if;
      tictoc20_pipe := tictoc20;
    end if;
  end if;
  
  
  
end process;



end Behavioral;
