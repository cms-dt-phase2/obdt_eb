----------------------------------------------------------------------------------
-- QuickWord FIFO
-- It's to FWFT fifos what FWFT are to regular ones.
-- Pre-reads one more word so that you have it available sooner
-- Alvaro Navarro, CIEMAT, 2018
----------------------------------------------------------------------------------
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.NUMERIC_STD.ALL;

library work;

entity pf_qwfifo is
    Generic (
        DATA_WIDTH : natural := 8;
        --DEPTH_LOG2 : natural := 11;
        STAGE : natural := 1
        );
    port (
        RST           : in std_logic;

        WRCLK         : in std_logic;
        WREN          : in std_logic;
        FULL          : out std_logic;
        DO            : out std_logic_vector(DATA_WIDTH-1 downto 0);
        
        RDCLK         : in std_logic;
        RDEN          : in std_logic;
        EMPTY         : out std_logic;
        DI            : in std_logic_vector(DATA_WIDTH-1 downto 0)
       );
       
end pf_qwfifo;

architecture Behavioral of pf_qwfifo is

    signal ff_rden, ff_empty, ff_dvld, empty_held : std_logic := '0';
    --signal ff_do, do_held : std_logic_vector(DATA_WIDTH-1 downto 0) := (others => '0');
    signal ff_di, ff_do, do_held : std_logic_vector(14 downto 0) := (others => '0');


begin


    corefifo_inst : entity work.testfifo
    port map (
        RESET => RST,                   -- 1-bit input reset
        
        WCLOCK => WRCLK,               -- 1-bit input write clock
        FULL => FULL,                 -- 1-bit output full
        we => WREN,                  -- 1-bit input write enable
        DATA => ff_di,                    -- Input data, width defined by DATA_WIDTH parameter
        
        RCLOCK => RDCLK,        -- 1-bit input read clock
        EMPTY => ff_empty,       -- 1-bit output empty
        DVLD => ff_dvld,
        RE => ff_rden,         -- 1-bit input read enable
        Q => ff_do              -- Output data, width defined by DATA_WIDTH parameter
    );

    ff_di(DATA_WIDTH - 1 downto 0) <= DI;

    ff_rden <= RDEN or empty_held;
    
    DO <= ff_do(DATA_WIDTH-1 downto 0) when ff_rden = '1' else do_held(DATA_WIDTH-1 downto 0);
    EMPTY <= not ff_dvld when ff_rden = '1' else empty_held;
    
    process(RDCLK)
    begin
        if rising_edge(RDCLK) then
            if RST = '1' then
                do_held <= (others => '0');
                empty_held <= '1';
            else
                if ff_rden = '1' then
                    do_held <= ff_do;
                    empty_held <= not ff_dvld;
                end if;
            end if;
         end if;
    end process;

end Behavioral;
