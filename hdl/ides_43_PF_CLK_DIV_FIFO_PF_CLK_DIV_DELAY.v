`timescale 1 ns/100 ps
// Version: PolarFire v2.3 12.200.35.9


module ides_43_PF_CLK_DIV_FIFO_PF_CLK_DIV_DELAY(
       CLK_IN,
       CLK_OUT,
       CLK_DIV_OUT
    );
input  CLK_IN;
output CLK_OUT;
output CLK_DIV_OUT;

    wire GND_net, VCC_net;
    
    ICB_CLKDIVDELAY #( .DIVIDER(3'b101), .DELAY_LINE_EN(1'b1), .DELAY_LINE_VAL(8'b00000000)
        , .DELAY_VAL_X2(1'b1), .FB_SOURCE_SEL_0(1'b0), .FB_SOURCE_SEL_1(1'b1)
         )  I_CDD (.DELAY_LINE_OUT_OF_RANGE(), .DELAY_LINE_DIR(GND_net)
        , .DELAY_LINE_MOVE(GND_net), .DELAY_LINE_LOAD(GND_net), .RST_N(
        VCC_net), .BIT_SLIP(GND_net), .A(CLK_IN), .Y_DIV(CLK_DIV_OUT), 
        .Y(CLK_OUT), .Y_FB());
    VCC vcc_inst (.Y(VCC_net));
    GND gnd_inst (.Y(GND_net));
    
endmodule
