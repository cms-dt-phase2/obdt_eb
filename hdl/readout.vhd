----------------------------------------------------------------------------------
-- Company: 
-- Engineer: 
-- 
-- Create Date: 24.08.2016 15:40:09
-- Design Name: 
-- Module Name: readout - Behavioral
-- Project Name: 
-- Target Devices: 
-- Tool Versions: 
-- Description: 
-- 
-- Dependencies: 
-- 
-- Revision:
-- Revision 0.01 - File Created
-- Additional Comments:
-- 
----------------------------------------------------------------------------------


library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

-- Uncomment the following library declaration if using
-- arithmetic functions with Signed or Unsigned values
use IEEE.NUMERIC_STD.ALL;


use work.data_type.all;


entity readout is
    Port ( clk : in STD_LOGIC;
           rst : in STD_LOGIC;
           data_in : in tdc_enc_a;
           bx_cnt : in STD_LOGIC_VECTOR(11 downto 0);
           data_out : out STD_LOGIC_VECTOR(24 downto 0);
           strobe_out : out STD_LOGIC);
end readout;

architecture Behavioral of readout is


type data_in_s_t is array (0 to 97) of std_logic_vector(4 downto 0);
signal data_in_s, data_in_s1 : data_in_s_t := (others=>(others=>'0'));
type din_t is array (0 to 17) of std_logic_vector(24 downto 0);
signal din_1, dout_1 : din_t := (others=>(others=>'0'));
type rd_en_1_t is array (0 to 17) of std_logic_vector(5 downto 0);
signal rd_en_1 : rd_en_1_t := (others=>(others=>'0'));
signal wr_en_1, empty_1, rd_en_1_i : std_logic_vector(17 downto 0) :=(others=>'0');
signal din_2, dout_2 : din_t := (others=>(others=>'0')); 
signal wr_en_2, rd_en_2, empty_2 : std_logic_vector(17 downto 0) :=(others=>'0');
signal din_out : std_logic_vector(24 downto 0) :=(others=>'0');
signal wr_en_out, rd_en_out, empty_out : std_logic :='0';
type cnt_t is array (0 to 17) of integer range 0 to 5;
signal cnt_1, cnt_2 : cnt_t :=(others=>0);
signal cnt_out : integer range 0 to 2 :=0;

begin

--in_g : for i in 0 to TDC_N-1 generate
    --data_in_s(i) <= data_in(i);
--end generate;

clk_domain_change : process(clk, data_in)
begin
    if rising_edge(clk) then
        for i in 0 to TDC_N-1 loop
            data_in_s1(i) <= data_in(i);
            data_in_s(i) <= data_in_s1(i);
        end loop;
    end if;
end process;

fifo_1_g: for i in 0 to 13 generate
    fifo_1 : entity work.ro_fifo
    PORT MAP (
        CLK   => clk,
        DATA  => din_1(i),
        RE    => rd_en_1_i(i),
        RESET => rst,
        WE    => wr_en_1(i),
        -- Outputs
        EMPTY => empty_1(i),
        FULL  => open,
        Q     => dout_1(i)
    );
end generate;

empty_1(17 downto 14) <= "1111";

fifo_2_g: for i in 0 to 2 generate
    fifo_1 : entity work.ro_fifo
    PORT MAP (
        CLK   => clk,
        DATA  => din_2(i),
        RE    => rd_en_2(i),
        RESET => rst,
        WE    => wr_en_2(i),
        -- Outputs
        EMPTY => empty_2(i),
        FULL  => open,
        Q     => dout_2(i)
    );
end generate;

fifo_out: entity work.ro_fifo
    PORT MAP (
        CLK   => clk,
        DATA  => din_out,
        RE    => rd_en_out,
        RESET => rst,
        WE    => wr_en_out,
        -- Outputs
        EMPTY => empty_out,
        FULL  => open,
        Q     => data_out
    );
    
write_1_g : for i in 0 to 13 generate
    write_1_p : process(clk, rst)
    begin
        if rst = '1' then
            din_1(i) <= (others=>'0');
            cnt_1(i) <= 0;
            wr_en_1(i) <= '0';
        elsif clk'event and clk = '1' then 
            if data_in_s(i*6+cnt_1(i)) /= "00000" then
                din_1(i) <= data_in_s(i*6+cnt_1(i)) & bx_cnt & std_logic_vector(to_unsigned(i*6+cnt_1(i),8));
                wr_en_1(i) <= '1';
            else
                din_1(i) <= din_1(i);
                wr_en_1(i) <= '0';
            end if;
            if cnt_1(i) >= 5 then
                cnt_1(i) <= 0;
            else
                cnt_1(i) <= cnt_1(i) + 1;
            end if;    
        end if;
    end process;
end generate;          

write_2_g : for i in 0 to 2 generate
    write_2_p : process(clk, rst)
    begin
        if rst = '1' then
            wr_en_2(i) <= '0';
            din_2(i) <= (others=>'0');
            cnt_2(i) <= 0;
            rd_en_1(i) <= (others=>'0');
        elsif clk'event and clk = '1' then    
            if cnt_2(i) = 0 then
                rd_en_1(i)(5) <= '0';
            else
                rd_en_1(i)(cnt_2(i)-1) <= '0';
            end if;
            if empty_1(i*6+cnt_2(i)) = '0' then
                din_2(i) <= dout_1(i*6+cnt_2(i));
                wr_en_2(i) <= '1';
                rd_en_1(i)(cnt_2(i)) <= '1';
            else
                din_2(i) <= din_2(i);
                wr_en_2(i) <= '0';
                rd_en_1(i)(cnt_2(i)) <= '0';
            end if;
            if cnt_2(i) >= 5 then
                cnt_2(i) <= 0;
            else
                cnt_2(i) <= cnt_2(i) + 1;
            end if;    
        end if;
    end process;
end generate;    

rd_en_1_g : for i in 0 to 17 generate
    rd_en_1_i(i) <= rd_en_1(i/6)(i mod 6);
end generate;

write_out_p : process(clk, rst)
begin
    if rst = '1' then
        wr_en_out <= '0';
        din_out <= (others=>'0');
        cnt_out <= 0;
        rd_en_2 <= (others=>'0');
    elsif clk'event and clk = '1' then
        if cnt_out = 0 then
            rd_en_2(2) <= '0';
        else
            rd_en_2(cnt_out-1) <= '0';
        end if;
        if empty_2(cnt_out) = '0' then
            din_out <= dout_2(cnt_out);
            wr_en_out <= '1';
            rd_en_2(cnt_out) <= '1';
        else
            din_out <= din_out;
            wr_en_out <= '0';
            rd_en_2(cnt_out) <= '0';
        end if;
        if cnt_out >= 2 then
            cnt_out <= 0;
        else
            cnt_out <= cnt_out + 1;
        end if;    
    end if;
end process;

--rd_en_out <= not empty_out;
rd_en_out_p : process(clk, empty_out)
begin
    if clk'event and clk = '1' then
        rd_en_out <= not empty_out;
    end if;
end process;

strobe_out <= rd_en_out;

end Behavioral;
