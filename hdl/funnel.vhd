----------------------------------------------------------------------------------
-- A. Navarro, 2018
-- funnel: basic concentration cell for the OBDT
-- 
-- Inputs are essentially QuickWord FIFOs with the data port separated into three fields:
-- bxlast (indicates last word for current bx), hit (indicates this word has a valid hit), and data (fine timestamp + address).
-- Note: QuickWord FIFO goes beyond FWFT: reduces 1 clock cycle the reading latency, at the cost of a little
-- extra combinatorial logic.
-- 
-- Output is a regular fifo write interface. In case it is full, no operation is performed,
-- so it will propagate the backpressure to the input data.
--
-- The algorithm reads every valid hit from the inputs corresponding to the current BX
-- (ie, until bxlast is active in all four channels) and then writes a bxlast to the output and
-- asserts read to all input fifos to continue with the next BX's data. The data from the lower-index
-- inputs is exhausted first, then it goes to the next input.
----------------------------------------------------------------------------------

library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.NUMERIC_STD.ALL;

package funnel_pkg is
  type busarr_t is array(natural range <>) of std_logic_vector;

  type bo2sl_t is array (boolean) of std_logic;
  constant bo2sl : bo2sl_t := (false => '0', true => '1');

  function f_log2 (x : positive) return natural;

end package funnel_pkg;

package body funnel_pkg is
  function f_log2 (x : positive) return natural is
    variable i : natural := 0;
  begin
    while (2**i < x) loop
      i := i + 1;
    end loop;
    return i;
  end function;
  
end package body funnel_pkg;


library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.NUMERIC_STD.ALL;

library work;
use work.funnel_pkg.ALL;

entity funnel is
  Generic (
    DATAIN_WIDTH : positive := 10; -- The width of the data field of the input ports; output port is 2 bits larger
    IPORT_NUM : positive := 4
    );
  Port (
    clk : in STD_LOGIC;
    rst : in STD_LOGIC;
    
    -- Input ports (QuickWord FIFOs)
    iport_data    : in  busarr_t(IPORT_NUM-1 downto 0)(DATAIN_WIDTH-1 downto 0);
    iport_hit     : in  std_logic_vector(IPORT_NUM-1 downto 0);
    iport_bxlast  : in  std_logic_vector(IPORT_NUM-1 downto 0);
    iport_empty   : in  std_logic_vector(IPORT_NUM-1 downto 0);
    iport_rden    : out std_logic_vector(IPORT_NUM-1 downto 0);

    -- Output port (FIFO writing)
    oport_data    : out std_logic_vector(DATAIN_WIDTH-1+f_log2(IPORT_NUM) downto 0);
    oport_hit     : out std_logic;
    oport_bxlast  : out std_logic;
    oport_full    : in std_logic;
    oport_wren    : out std_logic
    );

end funnel;

architecture Behavioral of funnel is

  constant address_bits : natural := f_log2(IPORT_NUM);

  function iport_sel_f (hits: std_logic_vector(IPORT_NUM-1 downto 0)) return integer is -- return value is range IPORT_NUM-1 downto -1
    variable result : integer range IPORT_NUM-1 downto -1 := -1;
  begin
    for index in IPORT_NUM-1 downto 0 loop
      if hits(index) = '1' then result := index;
      end if;
    end loop;
    return result;
  end function iport_sel_f;

begin


process(clk)
  variable iport_unused : std_logic_vector(IPORT_NUM-1 downto 0) := (others => '0');
  variable iport_sel : integer range IPORT_NUM-1 downto -1 := -1;
begin
  if rising_edge(clk) then
    if rst = '1' then
      iport_rden <= (others => '0');
      oport_wren <= '0';
      iport_unused := (others => '1');
    else

      iport_rden <= (others => '0');
      oport_wren <= '0';
      
      -- Whenever there is a word in every input, an output write will be done,
      -- iport_empty should always be either all 1's or all 0's, but it's better to enforce (all 0) than (any 0).
      if (iport_empty = (iport_empty'range => '0')) and (oport_full = '1') then
        oport_wren <= '1';
        
        -- Calculate if there's an input hit to write, and which iport it is in.
        iport_sel := iport_sel_f( iport_hit and iport_unused );

        -- If there's a valid bit, the written word will be a hit
        if iport_sel /= -1 then 
          oport_data <= std_logic_vector(to_unsigned(iport_sel, address_bits)) & iport_data(iport_sel);
          oport_hit <= '1';
          iport_rden(iport_sel) <= not iport_bxlast(iport_sel);
          iport_unused(iport_sel) := '0';
        else
          oport_data <= (others => '0') ;
          oport_hit <= '0';
        end if;
        
        -- If all inputs words tagged as this BX's last, and there is no valid hit remaining for next cycle
        -- (iport_sel_f reevaluated after possible update of iport_unused):
        -- then mark output word as BX's last and assert a read in all inputs, reset unused status for next BX.
        if ( iport_bxlast = (iport_bxlast'range => '1') ) and ( iport_sel_f(iport_hit and iport_unused) = -1 ) then
          oport_bxlast <= '1';
          iport_rden <= (others => '1');
          iport_unused := (others => '1');
        else 
          oport_bxlast <= '0';
        end if;

      end if;
    end if;
  end if;
end process;


end Behavioral;
