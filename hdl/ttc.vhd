----------------------------------------------------------------------------------
-- A. Navarro, 2018
-- 
-- TTC module
-- 
-- 
-- 
-- 
-- 
-- 
-- 
--
-- 
-- 
-- 
-- 
----------------------------------------------------------------------------------

library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.NUMERIC_STD.ALL;

package ttc_pkg is
  constant LHC_BUNCH_COUNT: integer := 3564;
end package;


library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.NUMERIC_STD.ALL;

library work;
use work.ttc_pkg.all;

entity ttc is
  Port ( 
    lhc_clk : in std_logic;
    rst     : in std_logic;
    bx_cnt  : out std_logic_vector(11 downto 0)
  );
end ttc;

architecture Behavioral of ttc is

begin

process(lhc_clk)
  variable bunch_ctr : unsigned(11 downto 0);
begin
  if rising_edge(lhc_clk) then
    if rst = '1' then
      bunch_ctr := (others => '0');
      bx_cnt <= std_logic_vector(bunch_ctr) ;
    else
      bunch_ctr := bunch_ctr + 1;
      if bunch_ctr = LHC_BUNCH_COUNT then bunch_ctr := (others => '0');
      end if;
      bx_cnt <= std_logic_vector(bunch_ctr) ;
    end if;
  end if;
end process;

end Behavioral;
