set_device -family {PolarFire} -die {MPF300TS_ES}
read_verilog -mode verilog_2k {/home/dima/Projects/obdt_eb/component/work/ccc0/ccc0_0/ccc0_ccc0_0_PF_CCC.v}
read_verilog -mode verilog_2k {/home/dima/Projects/obdt_eb/component/work/ccc0/ccc0.v}
read_verilog -mode verilog_2k {/home/dima/Projects/obdt_eb/component/work/ccc1/ccc1_0/ccc1_ccc1_0_PF_CCC.v}
read_verilog -mode verilog_2k {/home/dima/Projects/obdt_eb/component/work/ccc1/ccc1.v}
read_verilog -mode verilog_2k {/home/dima/Projects/obdt_eb/hdl/ides_19_PF_CLK_DIV_FIFO_PF_CLK_DIV_DELAY.v}
read_verilog -mode verilog_2k {/home/dima/Projects/obdt_eb/hdl/ides_19_PF_CLK_DIV_RXCLK_PF_CLK_DIV_DELAY.v}
read_verilog -mode verilog_2k {/home/dima/Projects/obdt_eb/hdl/ides_19_PF_IOD_RX_PF_IOD.v}
read_verilog -mode verilog_2k {/home/dima/Projects/obdt_eb/hdl/ides_19_PF_LANECTRL_0_PF_LANECTRL.v}
read_verilog -mode verilog_2k {/home/dima/Projects/obdt_eb/hdl/ides_19.v}
read_verilog -mode verilog_2k {/home/dima/Projects/obdt_eb/hdl/ides_36_PF_CLK_DIV_FIFO_PF_CLK_DIV_DELAY.v}
read_verilog -mode verilog_2k {/home/dima/Projects/obdt_eb/hdl/ides_36_PF_CLK_DIV_RXCLK_PF_CLK_DIV_DELAY.v}
read_verilog -mode verilog_2k {/home/dima/Projects/obdt_eb/hdl/ides_36_PF_IOD_RX_PF_IOD.v}
read_verilog -mode verilog_2k {/home/dima/Projects/obdt_eb/hdl/ides_36_PF_LANECTRL_0_PF_LANECTRL.v}
read_verilog -mode verilog_2k {/home/dima/Projects/obdt_eb/hdl/ides_36.v}
read_verilog -mode verilog_2k {/home/dima/Projects/obdt_eb/hdl/ides_43_PF_CLK_DIV_FIFO_PF_CLK_DIV_DELAY.v}
read_verilog -mode verilog_2k {/home/dima/Projects/obdt_eb/hdl/ides_43_PF_CLK_DIV_RXCLK_PF_CLK_DIV_DELAY.v}
read_verilog -mode verilog_2k {/home/dima/Projects/obdt_eb/hdl/ides_43_PF_IOD_RX_PF_IOD.v}
read_verilog -mode verilog_2k {/home/dima/Projects/obdt_eb/hdl/ides_43_PF_LANECTRL_0_PF_LANECTRL.v}
read_verilog -mode verilog_2k {/home/dima/Projects/obdt_eb/hdl/ides_43.v}
read_verilog -mode verilog_2k {/home/dima/Projects/obdt_eb/hdl/ides_46_PF_CLK_DIV_FIFO_PF_CLK_DIV_DELAY.v}
read_verilog -mode verilog_2k {/home/dima/Projects/obdt_eb/hdl/ides_46_PF_CLK_DIV_RXCLK_PF_CLK_DIV_DELAY.v}
read_verilog -mode verilog_2k {/home/dima/Projects/obdt_eb/hdl/ides_46_PF_IOD_RX_PF_IOD.v}
read_verilog -mode verilog_2k {/home/dima/Projects/obdt_eb/hdl/ides_46_PF_LANECTRL_0_PF_LANECTRL.v}
read_verilog -mode verilog_2k {/home/dima/Projects/obdt_eb/hdl/ides_46.v}
read_vhdl -mode vhdl_2008 {/home/dima/Projects/obdt_eb/hdl/data_type.vhd}
read_vhdl -mode vhdl_2008 {/home/dima/Projects/obdt_eb/hdl/tdc.vhd}
read_vhdl -mode vhdl_2008 {/home/dima/Projects/obdt_eb/hdl/ttc.vhd}
read_vhdl -mode vhdl_2008 {/home/dima/Projects/obdt_eb/hdl/top.vhd}
set_top_level {top}
read_sdc -component {/home/dima/Projects/obdt_eb/component/work/ccc1/ccc1_0/ccc1_ccc1_0_PF_CCC.sdc}
read_sdc -component {/home/dima/Projects/obdt_eb/component/work/ccc0/ccc0_0/ccc0_ccc0_0_PF_CCC.sdc}
derive_constraints
write_sdc {/home/dima/Projects/obdt_eb/constraint/top_derived_constraints.sdc}
write_pdc {/home/dima/Projects/obdt_eb/constraint/fp/top_derived_constraints.pdc}
