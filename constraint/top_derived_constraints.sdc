# Microsemi Corp.
# Date: 2019-Feb-10 19:43:28
# This file was generated based on the following SDC source files:
#   /home/dima/Projects/obdt_eb/component/work/ccc0/ccc0_0/ccc0_ccc0_0_PF_CCC.sdc
#   /home/dima/Projects/obdt_eb/component/work/ccc1/ccc1_0/ccc1_ccc1_0_PF_CCC.sdc
#

create_clock -name {clk_in_n} -period 20 [ get_ports { clk_in_n } ]
create_clock -name {tdc_inst/des_43_bank2/PF_CLK_DIV_FIFO/I_CDD/Y_DIV} -period 8.33333 [ get_pins { tdc_inst/des_43_bank2/PF_CLK_DIV_FIFO/I_CDD/Y_DIV } ]
create_generated_clock -name {tdc_inst/ccc_0/ccc0_0/pll_inst_0/OUT0} -multiply_by 12 -source [ get_pins { tdc_inst/ccc_0/ccc0_0/pll_inst_0/REF_CLK_0 } ] -phase 0 [ get_pins { tdc_inst/ccc_0/ccc0_0/pll_inst_0/OUT0 } ]
create_generated_clock -name {tdc_inst/ccc_1/ccc1_0/pll_inst_0/OUT0} -divide_by 3 -source [ get_pins { tdc_inst/ccc_1/ccc1_0/pll_inst_0/REF_CLK_0 } ] -phase 0 [ get_pins { tdc_inst/ccc_1/ccc1_0/pll_inst_0/OUT0 } ]
create_generated_clock -name {tdc_inst/ccc_1/ccc1_0/pll_inst_0/OUT2} -divide_by 1 -source [ get_pins { tdc_inst/ccc_1/ccc1_0/pll_inst_0/REF_CLK_0 } ] -phase 0 [ get_pins { tdc_inst/ccc_1/ccc1_0/pll_inst_0/OUT2 } ]
