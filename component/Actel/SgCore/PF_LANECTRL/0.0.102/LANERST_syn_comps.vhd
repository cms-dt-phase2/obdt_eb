-- Version: 
-- VHDL Black Box file 
-- 

library ieee;
use ieee.std_logic_1164.all;
library polarfire;
use polarfire.all;

entity LANERST is
   port( 
       RESET : in std_logic;
       ARST_N : out std_logic
   );
end LANERST;
architecture DEF_ARCH of LANERST is 

   attribute syn_black_box : boolean;
   attribute syn_black_box of DEF_ARCH : architecture is true;
   attribute black_box_pad_pin : string;
   attribute black_box_pad_pin of DEF_ARCH : architecture is "";

begin

end DEF_ARCH;
