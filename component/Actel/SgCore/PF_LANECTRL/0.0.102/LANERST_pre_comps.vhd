-- Version: 
-- VHDL Black Box file 
-- 

library ieee;
use ieee.std_logic_1164.all;
library polarfire;
use polarfire.all;

entity LANERST is
   port( 
       RESET : in std_logic;
       ARST_N : out std_logic
   );
end LANERST;
architecture DEF_ARCH of LANERST is 

   attribute black_box : boolean;
   attribute black_box of DEF_ARCH : architecture is true;
   attribute black_box_pad : string;
   attribute black_box_pad of DEF_ARCH : architecture is "";

begin

end DEF_ARCH;
