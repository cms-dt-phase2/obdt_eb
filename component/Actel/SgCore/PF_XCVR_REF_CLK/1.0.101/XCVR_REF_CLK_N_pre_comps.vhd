-- Version: 
-- VHDL Black Box file 
-- 

library ieee;
use ieee.std_logic_1164.all;
library polarfire;
use polarfire.all;

entity XCVR_REF_CLK_N is
	generic (
		IOSTD:string := "";
		CLKBUF_DUALCLK1_MODE:std_logic_vector := x"0";
		CLKBUF_DUALCLK1_ENTERM:std_logic_vector := x"0";
		CLKBUF_DUALCLK0_MODE:std_logic_vector := x"0";
		CLKBUF_DUALCLK0_ENTERM:std_logic_vector := x"0";
		CLKBUF_DUALCLK1_EN_HYST:std_logic_vector := x"0";
		CLKBUF_DUALCLK0_EN_HYST:std_logic_vector := x"0";
		CLKBUF_CLKBUF_EN_RDIFF:std_logic_vector := x"0";
		CLKBUF_CLKBUF_EN_UDRIVE_N:std_logic_vector := x"0";
		CLKBUF_CLKBUF_EN_UDRIVE_P:std_logic_vector := x"0";
		CLKBUF_CLKBUF_EN_PULLUP:std_logic_vector := x"0";
		CLKBUF_CLKBUF_EN_APAD:std_logic_vector := x"0"	);
   port( 
       Y : out std_logic;
       PAD_N : in std_logic;
       REFCLK : out std_logic
   );
end XCVR_REF_CLK_N;
architecture DEF_ARCH of XCVR_REF_CLK_N is 

   attribute black_box : boolean;
   attribute black_box of DEF_ARCH : architecture is true;
   attribute ment_tpd0: string;
   attribute ment_tpd0 of DEF_ARCH : architecture is " PAD_N->REFCLK=1.384";
   attribute ment_tpd1: string;
   attribute ment_tpd1 of DEF_ARCH : architecture is " PAD_N->Y=1.384";
   attribute black_box_pad : string;
   attribute black_box_pad of DEF_ARCH : architecture is "PAD_N";

begin

end DEF_ARCH;
