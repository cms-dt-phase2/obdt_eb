-- Version: 
-- VHDL Black Box file 
-- 

library ieee;
use ieee.std_logic_1164.all;
library polarfire;
use polarfire.all;

entity PLL is
	generic (
		VCOFREQUENCY:real := 0.0;
		DELAY_LINE_SIMULATION_MODE:string := "";
		DATA_RATE:real := 0.0;
		FORMAL_NAME:string := "";
		INTERFACE_NAME:string := "";
		INTERFACE_LEVEL:std_logic_vector := x"0";
		SOFTRESET:std_logic_vector := x"0";
		SOFT_POWERDOWN_N:std_logic_vector := x"0";
		RFDIV_EN:std_logic_vector := x"0";
		OUT0_DIV_EN:std_logic_vector := x"0";
		OUT1_DIV_EN:std_logic_vector := x"0";
		OUT2_DIV_EN:std_logic_vector := x"0";
		OUT3_DIV_EN:std_logic_vector := x"0";
		SOFT_REF_CLK_SEL:std_logic_vector := x"0";
		RESET_ON_LOCK:std_logic_vector := x"0";
		BYPASS_CLK_SEL:std_logic_vector := x"0";
		BYPASS_GO_EN_N:std_logic_vector := x"0";
		BYPASS_PLL:std_logic_vector := x"0";
		BYPASS_OUT_DIVIDER:std_logic_vector := x"0";
		FF_REQUIRES_LOCK:std_logic_vector := x"0";
		FSE_N:std_logic_vector := x"0";
		FB_CLK_SEL_0:std_logic_vector := x"0";
		FB_CLK_SEL_1:std_logic_vector := x"0";
		RFDIV:std_logic_vector := x"0";
		FRAC_EN:std_logic_vector := x"0";
		FRAC_DAC_EN:std_logic_vector := x"0";
		DIV0_RST_DELAY:std_logic_vector := x"0";
		DIV0_VAL:std_logic_vector := x"0";
		DIV1_RST_DELAY:std_logic_vector := x"0";
		DIV1_VAL:std_logic_vector := x"0";
		DIV2_RST_DELAY:std_logic_vector := x"0";
		DIV2_VAL:std_logic_vector := x"0";
		DIV3_RST_DELAY:std_logic_vector := x"0";
		DIV3_VAL:std_logic_vector := x"0";
		DIV3_CLK_SEL:std_logic_vector := x"0";
		BW_INT_CTRL:std_logic_vector := x"0";
		BW_PROP_CTRL:std_logic_vector := x"0";
		IREF_EN:std_logic_vector := x"0";
		IREF_TOGGLE:std_logic_vector := x"0";
		LOCK_CNT:std_logic_vector := x"0";
		DESKEW_CAL_CNT:std_logic_vector := x"0";
		DESKEW_CAL_EN:std_logic_vector := x"0";
		DESKEW_CAL_BYPASS:std_logic_vector := x"0";
		SYNC_REF_DIV_EN:std_logic_vector := x"0";
		SYNC_REF_DIV_EN_2:std_logic_vector := x"0";
		OUT0_PHASE_SEL:std_logic_vector := x"0";
		OUT1_PHASE_SEL:std_logic_vector := x"0";
		OUT2_PHASE_SEL:std_logic_vector := x"0";
		OUT3_PHASE_SEL:std_logic_vector := x"0";
		SOFT_LOAD_PHASE_N:std_logic_vector := x"0";
		SSM_DIV_VAL:std_logic_vector := x"0";
		FB_FRAC_VAL:std_logic_vector := x"0";
		SSM_SPREAD_MODE:std_logic_vector := x"0";
		SSM_MODULATION:std_logic_vector := x"0";
		FB_INT_VAL:std_logic_vector := x"0";
		SSM_EN_N:std_logic_vector := x"0";
		SSM_EXT_WAVE_EN:std_logic_vector := x"0";
		SSM_EXT_WAVE_MAX_ADDR:std_logic_vector := x"0";
		SSM_RANDOM_EN:std_logic_vector := x"0";
		SSM_RANDOM_PATTERN_SEL:std_logic_vector := x"0";
		CDMUX0_SEL:std_logic_vector := x"0";
		CDMUX1_SEL:std_logic_vector := x"0";
		CDMUX2_SEL:std_logic_vector := x"0";
		CDELAY0_SEL:std_logic_vector := x"0";
		CDELAY0_EN:std_logic_vector := x"0";
		DRI_EN:std_logic_vector := x"0"	);
   port( 
       POWERDOWN_N : in std_logic;
       OUT0_EN : in std_logic;
       OUT1_EN : in std_logic;
       OUT2_EN : in std_logic;
       OUT3_EN : in std_logic;
       REF_CLK_SEL : in std_logic;
       BYPASS_EN_N : in std_logic;
       LOAD_PHASE_N : in std_logic;
       SSCG_WAVE_TABLE : in std_logic_vector(7 downto 0);
       PHASE_DIRECTION : in std_logic;
       PHASE_ROTATE : in std_logic;
       PHASE_OUT0_SEL : in std_logic;
       PHASE_OUT1_SEL : in std_logic;
       PHASE_OUT2_SEL : in std_logic;
       PHASE_OUT3_SEL : in std_logic;
       DELAY_LINE_MOVE : in std_logic;
       DELAY_LINE_DIRECTION : in std_logic;
       DELAY_LINE_WIDE : in std_logic;
       DELAY_LINE_LOAD : in std_logic;
       LOCK : out std_logic;
       SSCG_WAVE_TABLE_ADDR : out std_logic_vector(7 downto 0);
       DELAY_LINE_OUT_OF_RANGE : out std_logic;
       REFCLK_SYNC_EN : in std_logic;
       REF_CLK_0 : in std_logic;
       REF_CLK_1 : in std_logic;
       FB_CLK : in std_logic;
       OUT0 : out std_logic;
       OUT1 : out std_logic;
       OUT2 : out std_logic;
       OUT3 : out std_logic;
       DRI_CLK : in std_logic;
       DRI_CTRL : in std_logic_vector(10 downto 0);
       DRI_WDATA : in std_logic_vector(32 downto 0);
       DRI_ARST_N : in std_logic;
       DRI_RDATA : out std_logic_vector(32 downto 0);
       DRI_INTERRUPT : out std_logic
   );
end PLL;
architecture DEF_ARCH of PLL is 

   attribute black_box : boolean;
   attribute black_box of DEF_ARCH : architecture is true;
   attribute ment_tco0: string;
   attribute ment_tco0 of DEF_ARCH : architecture is " REF_CLK_0->OUT0=1.384";
   attribute ment_tco1: string;
   attribute ment_tco1 of DEF_ARCH : architecture is " REF_CLK_0->OUT1=1.384";
   attribute ment_tco2: string;
   attribute ment_tco2 of DEF_ARCH : architecture is " REF_CLK_0->OUT2=1.384";
   attribute ment_tco3: string;
   attribute ment_tco3 of DEF_ARCH : architecture is " REF_CLK_0->OUT3=1.384";
   attribute ment_tco4: string;
   attribute ment_tco4 of DEF_ARCH : architecture is " REF_CLK_1->OUT0=1.384";
   attribute ment_tco5: string;
   attribute ment_tco5 of DEF_ARCH : architecture is " REF_CLK_1->OUT1=1.384";
   attribute ment_tco6: string;
   attribute ment_tco6 of DEF_ARCH : architecture is " REF_CLK_1->OUT2=1.384";
   attribute ment_tco7: string;
   attribute ment_tco7 of DEF_ARCH : architecture is " REF_CLK_1->OUT3=1.384";
   attribute black_box_pad : string;
   attribute black_box_pad of DEF_ARCH : architecture is "";

begin

end DEF_ARCH;
