-- Version: 
-- VHDL Black Box file 
-- 

library ieee;
use ieee.std_logic_1164.all;
library polarfire;
use polarfire.all;

entity HS_IO_CLK is
   port( 
       A : in std_logic;
       Y : out std_logic
   );
end HS_IO_CLK;
architecture DEF_ARCH of HS_IO_CLK is 

   attribute black_box : boolean;
   attribute black_box of DEF_ARCH : architecture is true;
   attribute ment_tpd0: string;
   attribute ment_tpd0 of DEF_ARCH : architecture is " A->Y=1.384";
   attribute black_box_pad : string;
   attribute black_box_pad of DEF_ARCH : architecture is "";

begin

end DEF_ARCH;
