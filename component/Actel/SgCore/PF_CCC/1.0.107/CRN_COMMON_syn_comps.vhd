-- Version: 
-- VHDL Black Box file 
-- 

library ieee;
use ieee.std_logic_1164.all;
library polarfire;
use polarfire.all;

entity CRN_COMMON is
	generic (
		PC_CIMUX_GEN0_SEL:std_logic_vector := x"0";
		PC_CIMUX_GEN1_SEL:std_logic_vector := x"0";
		PC_CIMUX_GEN2_SEL:std_logic_vector := x"0";
		PC_CIMUX_GEN3_SEL:std_logic_vector := x"0";
		PC_CIMUX_GEN4_SEL:std_logic_vector := x"0";
		PC_CIMUX_GEN5_SEL:std_logic_vector := x"0";
		PC_CIMUX_REF0_SEL:std_logic_vector := x"0";
		PC_CIMUX_REF1_SEL:std_logic_vector := x"0";
		PC_CIMUX_REF2_SEL:std_logic_vector := x"0";
		PC_CIMUX_REF3_SEL:std_logic_vector := x"0";
		PC_CIMUX_REF4_SEL:std_logic_vector := x"0";
		PC_CIMUX_REF5_SEL:std_logic_vector := x"0";
		PC_CIMUX_REF6_SEL:std_logic_vector := x"0";
		PC_CIMUX_REF7_SEL:std_logic_vector := x"0";
		PC_CIMUX_REF8_SEL:std_logic_vector := x"0";
		PC_CIMUX_REF9_SEL:std_logic_vector := x"0";
		PC_CDMUX6_SEL:std_logic_vector := x"0";
		PC_COMUX0_SEL:std_logic_vector := x"0";
		PC_COMUX1_SEL:std_logic_vector := x"0";
		PC_COMUX2_SEL:std_logic_vector := x"0";
		PC_COMUX3_SEL:std_logic_vector := x"0";
		PC_COMUX4_SEL:std_logic_vector := x"0";
		PC_COMUX5_SEL:std_logic_vector := x"0";
		PC_COMUX6_SEL:std_logic_vector := x"0";
		PC_COMUX7_SEL:std_logic_vector := x"0";
		PC_SRMUX0_SEL:std_logic_vector := x"0";
		PC_SRMUX1_SEL:std_logic_vector := x"0";
		PC_CRNFDR_HORZ_EN:std_logic_vector := x"0";
		PC_CRNFR_VERT_EN:std_logic_vector := x"0";
		PC_SOFTRESET:std_logic_vector := x"0";
		PC_DYN_EN:std_logic_vector := x"0"	);
   port( 
       REFCLK_SYNC_EN : in std_logic;
       CFM_CLKOUT : out std_logic;
       PLL0_REFCLK_SYNC_EN : out std_logic;
       PLL1_REFCLK_SYNC_EN : out std_logic
   );
end CRN_COMMON;
architecture DEF_ARCH of CRN_COMMON is 

   attribute syn_black_box : boolean;
   attribute syn_black_box of DEF_ARCH : architecture is true;
   attribute black_box_pad_pin : string;
   attribute black_box_pad_pin of DEF_ARCH : architecture is "";

begin

end DEF_ARCH;
