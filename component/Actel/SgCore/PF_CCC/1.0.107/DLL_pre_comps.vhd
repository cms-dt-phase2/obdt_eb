-- Version: 
-- VHDL Black Box file 
-- 

library ieee;
use ieee.std_logic_1164.all;
library polarfire;
use polarfire.all;

entity DLL is
	generic (
		DATA_RATE:real := 0.0;
		FORMAL_NAME:string := "";
		INTERFACE_NAME:string := "";
		INTERFACE_LEVEL:std_logic_vector := x"0";
		SOFTRESET:std_logic_vector := x"0";
		PRIMARY_PHASE:std_logic_vector := x"0";
		SECONDARY_PHASE:std_logic_vector := x"0";
		PRIMARY_CLK_SEL:std_logic_vector := x"0";
		SECONDARY_CLK_SEL:std_logic_vector := x"0";
		REF_SEL:std_logic_vector := x"0";
		FB_SEL:std_logic_vector := x"0";
		DIV_SEL:std_logic_vector := x"0";
		SECONDARY_FINE_PHASE:std_logic_vector := x"0";
		ALU_UPDATE:std_logic_vector := x"0";
		PHASE_CODE_SEL:std_logic_vector := x"0";
		LOCK_TOLERANCE:std_logic_vector := x"0";
		LOCK_HIGH:std_logic_vector := x"0";
		LOCK_LOW:std_logic_vector := x"0";
		SET_ALU:std_logic_vector := x"0";
		ADJ_DEL4:std_logic_vector := x"0";
		RESERVED_0:std_logic_vector := x"0";
		ADJ_CODE:std_logic_vector := x"0";
		INIT_CODE:std_logic_vector := x"0";
		FAST_RELOCK:std_logic_vector := x"0";
		POWERDOWN_EN:std_logic_vector := x"0";
		RESET:std_logic_vector := x"0";
		SOFT_ALU_HOLD:std_logic_vector := x"0";
		SOFT_CODE_UPDATE:std_logic_vector := x"0";
		SOFT_LOCK_DBG:std_logic_vector := x"0";
		SOFT_LOCK_FRC:std_logic_vector := x"0";
		SOFT_PHASE_DIRECTION:std_logic_vector := x"0";
		SOFT_PHASE_LOAD:std_logic_vector := x"0";
		SOFT_PHASE_MOVE_CLK:std_logic_vector := x"0";
		SOFT_MOVE_CODE:std_logic_vector := x"0";
		TEST_RING:std_logic_vector := x"0";
		DELAY_DIFF_RANGE:std_logic_vector := x"0";
		DRI_EN:std_logic_vector := x"0"	);
   port( 
       POWERDOWN_N : in std_logic;
       DIR : in std_logic;
       CLK_MOVE : in std_logic;
       CODE_MOVE : in std_logic;
       ALU_HOLD : in std_logic;
       CODE_UPDATE : in std_logic;
       PHASE_LOAD : in std_logic;
       CLK_MOVE_DONE : out std_logic;
       CODE_MOVE_DONE : out std_logic;
       LOCK : out std_logic;
       DELAY_DIFF : out std_logic;
       CODE : out std_logic_vector(7 downto 0);
       REF_CLK : in std_logic;
       FB_CLK : in std_logic;
       CLK_0 : out std_logic;
       CLK_1 : out std_logic;
       DRI_CLK : in std_logic;
       DRI_CTRL : in std_logic_vector(10 downto 0);
       DRI_WDATA : in std_logic_vector(32 downto 0);
       DRI_ARST_N : in std_logic;
       DRI_RDATA : out std_logic_vector(32 downto 0);
       DRI_INTERRUPT : out std_logic
   );
end DLL;
architecture DEF_ARCH of DLL is 

   attribute black_box : boolean;
   attribute black_box of DEF_ARCH : architecture is true;
   attribute ment_tpd0: string;
   attribute ment_tpd0 of DEF_ARCH : architecture is " FB_CLK->CLK_0=1.384";
   attribute ment_tpd1: string;
   attribute ment_tpd1 of DEF_ARCH : architecture is " FB_CLK->CLK_1=1.384";
   attribute ment_tpd2: string;
   attribute ment_tpd2 of DEF_ARCH : architecture is " REF_CLK->CLK_0=1.384";
   attribute ment_tpd3: string;
   attribute ment_tpd3 of DEF_ARCH : architecture is " REF_CLK->CLK_1=1.384";
   attribute black_box_pad : string;
   attribute black_box_pad of DEF_ARCH : architecture is "";

begin

end DEF_ARCH;
