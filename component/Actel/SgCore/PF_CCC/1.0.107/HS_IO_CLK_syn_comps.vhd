-- Version: 
-- VHDL Black Box file 
-- 

library ieee;
use ieee.std_logic_1164.all;
library polarfire;
use polarfire.all;

entity HS_IO_CLK is
   port( 
       A : in std_logic;
       Y : out std_logic
   );
end HS_IO_CLK;
architecture DEF_ARCH of HS_IO_CLK is 

   attribute syn_black_box : boolean;
   attribute syn_black_box of DEF_ARCH : architecture is true;
   attribute syn_tpd0: string;
   attribute syn_tpd0 of DEF_ARCH : architecture is " A->Y = 1.384";
   attribute black_box_pad_pin : string;
   attribute black_box_pad_pin of DEF_ARCH : architecture is "";

begin

end DEF_ARCH;
