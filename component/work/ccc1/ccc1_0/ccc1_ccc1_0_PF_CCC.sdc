set_component ccc1_ccc1_0_PF_CCC
# Microsemi Corp.
# Date: 2019-Jan-09 10:16:54
#

# Base clock for PLL #0
create_clock -period 8.33333 [ get_pins { pll_inst_0/REF_CLK_0 } ]
create_generated_clock -divide_by 3 -source [ get_pins { pll_inst_0/REF_CLK_0 } ] -phase 0 [ get_pins { pll_inst_0/OUT0 } ]
create_generated_clock -multiply_by 2 -source [ get_pins { pll_inst_0/REF_CLK_0 } ] -phase 0 [ get_pins { pll_inst_0/OUT1 } ]
create_generated_clock -divide_by 1 -source [ get_pins { pll_inst_0/REF_CLK_0 } ] -phase 0 [ get_pins { pll_inst_0/OUT2 } ]
