-- Version: PolarFire v2.3 12.200.35.9

library ieee;
use ieee.std_logic_1164.all;
library polarfire;
use polarfire.all;

entity testfifo_testfifo_0_USRAM_top is

    port( R_DATA        : out   std_logic_vector(14 downto 0);
          W_DATA        : in    std_logic_vector(14 downto 0);
          R_ADDR        : in    std_logic_vector(5 downto 0);
          W_ADDR        : in    std_logic_vector(5 downto 0);
          BLK_EN        : in    std_logic;
          R_ADDR_ARST_N : in    std_logic;
          R_ADDR_SRST_N : in    std_logic;
          R_ADDR_EN     : in    std_logic;
          R_CLK         : in    std_logic;
          W_CLK         : in    std_logic;
          W_EN          : in    std_logic
        );

end testfifo_testfifo_0_USRAM_top;

architecture DEF_ARCH of testfifo_testfifo_0_USRAM_top is 

  component INV
    port( A : in    std_logic := 'U';
          Y : out   std_logic
        );
  end component;

  component RAM64x12
    generic (MEMORYFILE:string := ""; RAMINDEX:string := ""; 
        INIT0:std_logic_vector(63 downto 0) := (others => 'X'); 
        INIT1:std_logic_vector(63 downto 0) := (others => 'X'); 
        INIT2:std_logic_vector(63 downto 0) := (others => 'X'); 
        INIT3:std_logic_vector(63 downto 0) := (others => 'X'); 
        INIT4:std_logic_vector(63 downto 0) := (others => 'X'); 
        INIT5:std_logic_vector(63 downto 0) := (others => 'X'); 
        INIT6:std_logic_vector(63 downto 0) := (others => 'X'); 
        INIT7:std_logic_vector(63 downto 0) := (others => 'X'); 
        INIT8:std_logic_vector(63 downto 0) := (others => 'X'); 
        INIT9:std_logic_vector(63 downto 0) := (others => 'X'); 
        INIT10:std_logic_vector(63 downto 0) := (others => 'X'); 
        INIT11:std_logic_vector(63 downto 0) := (others => 'X')
        );

    port( BLK_EN        : in    std_logic := 'U';
          BUSY_FB       : in    std_logic := 'U';
          R_ADDR        : in    std_logic_vector(5 downto 0) := (others => 'U');
          R_ADDR_AD_N   : in    std_logic := 'U';
          R_ADDR_AL_N   : in    std_logic := 'U';
          R_ADDR_BYPASS : in    std_logic := 'U';
          R_ADDR_EN     : in    std_logic := 'U';
          R_ADDR_SD     : in    std_logic := 'U';
          R_ADDR_SL_N   : in    std_logic := 'U';
          R_CLK         : in    std_logic := 'U';
          R_DATA_AD_N   : in    std_logic := 'U';
          R_DATA_AL_N   : in    std_logic := 'U';
          R_DATA_BYPASS : in    std_logic := 'U';
          R_DATA_EN     : in    std_logic := 'U';
          R_DATA_SD     : in    std_logic := 'U';
          R_DATA_SL_N   : in    std_logic := 'U';
          W_ADDR        : in    std_logic_vector(5 downto 0) := (others => 'U');
          W_CLK         : in    std_logic := 'U';
          W_DATA        : in    std_logic_vector(11 downto 0) := (others => 'U');
          W_EN          : in    std_logic := 'U';
          ACCESS_BUSY   : out   std_logic;
          R_DATA        : out   std_logic_vector(11 downto 0)
        );
  end component;

  component GND
    port(Y : out std_logic); 
  end component;

  component VCC
    port(Y : out std_logic); 
  end component;

    signal ARSTADDRAP, SRSTADDRAP, \ACCESS_BUSY[0][0]\, 
        \ACCESS_BUSY[0][1]\, \VCC\, \GND\, ADLIB_VCC : std_logic;
    signal GND_power_net1 : std_logic;
    signal VCC_power_net1 : std_logic;
    signal nc8, nc7, nc6, nc2, nc9, nc5, nc4, nc3, nc1
         : std_logic;

begin 

    \GND\ <= GND_power_net1;
    \VCC\ <= VCC_power_net1;
    ADLIB_VCC <= VCC_power_net1;

    INVARSTADDRAP : INV
      port map(A => R_ADDR_ARST_N, Y => ARSTADDRAP);
    
    testfifo_testfifo_0_USRAM_top_R0C1 : RAM64x12
      generic map(RAMINDEX => "core%64%15%SPEED%0%1%MICRO_RAM")

      port map(BLK_EN => BLK_EN, BUSY_FB => \GND\, R_ADDR(5) => 
        R_ADDR(5), R_ADDR(4) => R_ADDR(4), R_ADDR(3) => R_ADDR(3), 
        R_ADDR(2) => R_ADDR(2), R_ADDR(1) => R_ADDR(1), R_ADDR(0)
         => R_ADDR(0), R_ADDR_AD_N => \VCC\, R_ADDR_AL_N => 
        ARSTADDRAP, R_ADDR_BYPASS => \GND\, R_ADDR_EN => 
        R_ADDR_EN, R_ADDR_SD => \GND\, R_ADDR_SL_N => SRSTADDRAP, 
        R_CLK => R_CLK, R_DATA_AD_N => \VCC\, R_DATA_AL_N => 
        \VCC\, R_DATA_BYPASS => \VCC\, R_DATA_EN => \VCC\, 
        R_DATA_SD => \GND\, R_DATA_SL_N => \VCC\, W_ADDR(5) => 
        W_ADDR(5), W_ADDR(4) => W_ADDR(4), W_ADDR(3) => W_ADDR(3), 
        W_ADDR(2) => W_ADDR(2), W_ADDR(1) => W_ADDR(1), W_ADDR(0)
         => W_ADDR(0), W_CLK => W_CLK, W_DATA(11) => \GND\, 
        W_DATA(10) => \GND\, W_DATA(9) => \GND\, W_DATA(8) => 
        \GND\, W_DATA(7) => \GND\, W_DATA(6) => \GND\, W_DATA(5)
         => \GND\, W_DATA(4) => \GND\, W_DATA(3) => \GND\, 
        W_DATA(2) => W_DATA(14), W_DATA(1) => W_DATA(13), 
        W_DATA(0) => W_DATA(12), W_EN => W_EN, ACCESS_BUSY => 
        \ACCESS_BUSY[0][1]\, R_DATA(11) => nc8, R_DATA(10) => nc7, 
        R_DATA(9) => nc6, R_DATA(8) => nc2, R_DATA(7) => nc9, 
        R_DATA(6) => nc5, R_DATA(5) => nc4, R_DATA(4) => nc3, 
        R_DATA(3) => nc1, R_DATA(2) => R_DATA(14), R_DATA(1) => 
        R_DATA(13), R_DATA(0) => R_DATA(12));
    
    testfifo_testfifo_0_USRAM_top_R0C0 : RAM64x12
      generic map(RAMINDEX => "core%64%15%SPEED%0%0%MICRO_RAM")

      port map(BLK_EN => BLK_EN, BUSY_FB => \GND\, R_ADDR(5) => 
        R_ADDR(5), R_ADDR(4) => R_ADDR(4), R_ADDR(3) => R_ADDR(3), 
        R_ADDR(2) => R_ADDR(2), R_ADDR(1) => R_ADDR(1), R_ADDR(0)
         => R_ADDR(0), R_ADDR_AD_N => \VCC\, R_ADDR_AL_N => 
        ARSTADDRAP, R_ADDR_BYPASS => \GND\, R_ADDR_EN => 
        R_ADDR_EN, R_ADDR_SD => \GND\, R_ADDR_SL_N => SRSTADDRAP, 
        R_CLK => R_CLK, R_DATA_AD_N => \VCC\, R_DATA_AL_N => 
        \VCC\, R_DATA_BYPASS => \VCC\, R_DATA_EN => \VCC\, 
        R_DATA_SD => \GND\, R_DATA_SL_N => \VCC\, W_ADDR(5) => 
        W_ADDR(5), W_ADDR(4) => W_ADDR(4), W_ADDR(3) => W_ADDR(3), 
        W_ADDR(2) => W_ADDR(2), W_ADDR(1) => W_ADDR(1), W_ADDR(0)
         => W_ADDR(0), W_CLK => W_CLK, W_DATA(11) => W_DATA(11), 
        W_DATA(10) => W_DATA(10), W_DATA(9) => W_DATA(9), 
        W_DATA(8) => W_DATA(8), W_DATA(7) => W_DATA(7), W_DATA(6)
         => W_DATA(6), W_DATA(5) => W_DATA(5), W_DATA(4) => 
        W_DATA(4), W_DATA(3) => W_DATA(3), W_DATA(2) => W_DATA(2), 
        W_DATA(1) => W_DATA(1), W_DATA(0) => W_DATA(0), W_EN => 
        W_EN, ACCESS_BUSY => \ACCESS_BUSY[0][0]\, R_DATA(11) => 
        R_DATA(11), R_DATA(10) => R_DATA(10), R_DATA(9) => 
        R_DATA(9), R_DATA(8) => R_DATA(8), R_DATA(7) => R_DATA(7), 
        R_DATA(6) => R_DATA(6), R_DATA(5) => R_DATA(5), R_DATA(4)
         => R_DATA(4), R_DATA(3) => R_DATA(3), R_DATA(2) => 
        R_DATA(2), R_DATA(1) => R_DATA(1), R_DATA(0) => R_DATA(0));
    
    INVSRSTADDRAP : INV
      port map(A => R_ADDR_SRST_N, Y => SRSTADDRAP);
    
    GND_power_inst1 : GND
      port map( Y => GND_power_net1);

    VCC_power_inst1 : VCC
      port map( Y => VCC_power_net1);


end DEF_ARCH; 
