----------------------------------------------------------------------
-- Created by SmartDesign Tue Dec 18 14:27:42 2018
-- Version: PolarFire v2.3 12.200.35.9
----------------------------------------------------------------------

----------------------------------------------------------------------
-- Libraries
----------------------------------------------------------------------
library ieee;
use ieee.std_logic_1164.all;

library polarfire;
use polarfire.all;
library COREFIFO_LIB;
use COREFIFO_LIB.all;
----------------------------------------------------------------------
-- testfifo entity declaration
----------------------------------------------------------------------
entity testfifo is
    -- Port list
    port(
        -- Inputs
        DATA   : in  std_logic_vector(14 downto 0);
        RCLOCK : in  std_logic;
        RE     : in  std_logic;
        RESET  : in  std_logic;
        WCLOCK : in  std_logic;
        WE     : in  std_logic;
        -- Outputs
        DVLD   : out std_logic;
        EMPTY  : out std_logic;
        FULL   : out std_logic;
        Q      : out std_logic_vector(14 downto 0)
        );
end testfifo;
----------------------------------------------------------------------
-- testfifo architecture body
----------------------------------------------------------------------
architecture RTL of testfifo is
----------------------------------------------------------------------
-- Component declarations
----------------------------------------------------------------------
-- testfifo_testfifo_0_COREFIFO   -   Actel:DirectCore:COREFIFO:2.7.105
component testfifo_testfifo_0_COREFIFO
    generic( 
        AE_STATIC_EN   : integer := 0 ;
        AEVAL          : integer := 4 ;
        AF_STATIC_EN   : integer := 0 ;
        AFVAL          : integer := 60 ;
        CTRL_TYPE      : integer := 3 ;
        ECC            : integer := 0 ;
        ESTOP          : integer := 1 ;
        FAMILY         : integer := 26 ;
        FSTOP          : integer := 1 ;
        FWFT           : integer := 1 ;
        NUM_STAGES     : integer := 2 ;
        OVERFLOW_EN    : integer := 0 ;
        PIPE           : integer := 1 ;
        PREFETCH       : integer := 0 ;
        RCLK_EDGE      : integer := 1 ;
        RDCNT_EN       : integer := 0 ;
        RDEPTH         : integer := 64 ;
        RE_POLARITY    : integer := 0 ;
        READ_DVALID    : integer := 1 ;
        RESET_POLARITY : integer := 1 ;
        RWIDTH         : integer := 15 ;
        SYNC           : integer := 0 ;
        UNDERFLOW_EN   : integer := 0 ;
        WCLK_EDGE      : integer := 1 ;
        WDEPTH         : integer := 64 ;
        WE_POLARITY    : integer := 0 ;
        WRCNT_EN       : integer := 0 ;
        WRITE_ACK      : integer := 0 ;
        WWIDTH         : integer := 15 
        );
    -- Port list
    port(
        -- Inputs
        CLK        : in  std_logic;
        DATA       : in  std_logic_vector(14 downto 0);
        MEMRD      : in  std_logic_vector(14 downto 0);
        RCLOCK     : in  std_logic;
        RE         : in  std_logic;
        RESET      : in  std_logic;
        WCLOCK     : in  std_logic;
        WE         : in  std_logic;
        -- Outputs
        AEMPTY     : out std_logic;
        AFULL      : out std_logic;
        DB_DETECT  : out std_logic;
        DVLD       : out std_logic;
        EMPTY      : out std_logic;
        FULL       : out std_logic;
        MEMRADDR   : out std_logic_vector(5 downto 0);
        MEMRE      : out std_logic;
        MEMWADDR   : out std_logic_vector(5 downto 0);
        MEMWD      : out std_logic_vector(14 downto 0);
        MEMWE      : out std_logic;
        OVERFLOW   : out std_logic;
        Q          : out std_logic_vector(14 downto 0);
        RDCNT      : out std_logic_vector(6 downto 0);
        SB_CORRECT : out std_logic;
        UNDERFLOW  : out std_logic;
        WACK       : out std_logic;
        WRCNT      : out std_logic_vector(6 downto 0)
        );
end component;
----------------------------------------------------------------------
-- Signal declarations
----------------------------------------------------------------------
signal DVLD_net_0  : std_logic;
signal EMPTY_net_0 : std_logic;
signal FULL_net_0  : std_logic;
signal Q_0         : std_logic_vector(14 downto 0);
signal FULL_net_1  : std_logic;
signal EMPTY_net_1 : std_logic;
signal Q_0_net_0   : std_logic_vector(14 downto 0);
signal DVLD_net_1  : std_logic;
----------------------------------------------------------------------
-- TiedOff Signals
----------------------------------------------------------------------
signal GND_net     : std_logic;
signal MEMRD_const_net_0: std_logic_vector(14 downto 0);

begin
----------------------------------------------------------------------
-- Constant assignments
----------------------------------------------------------------------
 GND_net           <= '0';
 MEMRD_const_net_0 <= B"000000000000000";
----------------------------------------------------------------------
-- Top level output port assignments
----------------------------------------------------------------------
 FULL_net_1     <= FULL_net_0;
 FULL           <= FULL_net_1;
 EMPTY_net_1    <= EMPTY_net_0;
 EMPTY          <= EMPTY_net_1;
 Q_0_net_0      <= Q_0;
 Q(14 downto 0) <= Q_0_net_0;
 DVLD_net_1     <= DVLD_net_0;
 DVLD           <= DVLD_net_1;
----------------------------------------------------------------------
-- Component instances
----------------------------------------------------------------------
-- testfifo_0   -   Actel:DirectCore:COREFIFO:2.7.105
testfifo_0 : testfifo_testfifo_0_COREFIFO
    generic map( 
        AE_STATIC_EN   => ( 0 ),
        AEVAL          => ( 4 ),
        AF_STATIC_EN   => ( 0 ),
        AFVAL          => ( 60 ),
        CTRL_TYPE      => ( 3 ),
        ECC            => ( 0 ),
        ESTOP          => ( 1 ),
        FAMILY         => ( 26 ),
        FSTOP          => ( 1 ),
        FWFT           => ( 1 ),
        NUM_STAGES     => ( 2 ),
        OVERFLOW_EN    => ( 0 ),
        PIPE           => ( 1 ),
        PREFETCH       => ( 0 ),
        RCLK_EDGE      => ( 1 ),
        RDCNT_EN       => ( 0 ),
        RDEPTH         => ( 64 ),
        RE_POLARITY    => ( 0 ),
        READ_DVALID    => ( 1 ),
        RESET_POLARITY => ( 1 ),
        RWIDTH         => ( 15 ),
        SYNC           => ( 0 ),
        UNDERFLOW_EN   => ( 0 ),
        WCLK_EDGE      => ( 1 ),
        WDEPTH         => ( 64 ),
        WE_POLARITY    => ( 0 ),
        WRCNT_EN       => ( 0 ),
        WRITE_ACK      => ( 0 ),
        WWIDTH         => ( 15 )
        )
    port map( 
        -- Inputs
        CLK        => GND_net, -- tied to '0' from definition
        WCLOCK     => WCLOCK,
        RCLOCK     => RCLOCK,
        RESET      => RESET,
        WE         => WE,
        RE         => RE,
        DATA       => DATA,
        MEMRD      => MEMRD_const_net_0, -- tied to X"0" from definition
        -- Outputs
        FULL       => FULL_net_0,
        EMPTY      => EMPTY_net_0,
        AFULL      => OPEN,
        AEMPTY     => OPEN,
        OVERFLOW   => OPEN,
        UNDERFLOW  => OPEN,
        WACK       => OPEN,
        DVLD       => DVLD_net_0,
        MEMWE      => OPEN,
        MEMRE      => OPEN,
        SB_CORRECT => OPEN,
        DB_DETECT  => OPEN,
        Q          => Q_0,
        WRCNT      => OPEN,
        RDCNT      => OPEN,
        MEMWADDR   => OPEN,
        MEMRADDR   => OPEN,
        MEMWD      => OPEN 
        );

end RTL;
