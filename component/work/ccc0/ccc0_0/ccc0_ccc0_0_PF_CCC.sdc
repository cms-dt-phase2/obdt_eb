set_component ccc0_ccc0_0_PF_CCC
# Microsemi Corp.
# Date: 2019-Jan-09 10:15:45
#

# Base clock for PLL #0
create_clock -period 20 [ get_pins { pll_inst_0/REF_CLK_0 } ]
create_generated_clock -multiply_by 12 -source [ get_pins { pll_inst_0/REF_CLK_0 } ] -phase 0 [ get_pins { pll_inst_0/OUT0 } ]
